/*
 * This file is part of mfield.
 * 
 * Copyright (C) 2013 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* mfield.c - a simple program to display a vector field showing the
 * magnetic field around conductors perpendicular to the plane. */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#if HAVE_ASSERT_H
# include <assert.h>
#endif /* HAVE_ASSERT_H */

#include <GLFW/glfw3.h>
#include <GL/glu.h>

#include <gtk/gtk.h>

#include <libconfig.h>

#if HAVE_LIMITS_H
# include <limits.h>
#endif /* HAVE_LIMITS_H */

#if HAVE_MATH_H
# include <math.h>
#endif /* HAVE_MATH_H */

#if HAVE_PTHREAD_H
# include <pthread.h>
#endif /* HAVE_PTHREAD_H */

#if HAVE_STDARG_H
# include <stdarg.h>
#endif /* HAVE_STDARG_H */

#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

#if HAVE_STDIO_H
# include <stdio.h>
#endif /* HAVE_STDIO_H */

#if HAVE_STDLIB_H
# include <stdlib.h>
#endif /* HAVE_STDLIB_H */

#if HAVE_STRING_H
# include <string.h>
#endif /* HAVE_STRING_H */

#if HAVE_UNISTD_H
# include <unistd.h>
#endif /* HAVE_UNISTD_H */

#if BUILDING_FROM_GIT
# include "git-commit.h"
#endif /* BUILDING_FROM_GIT */

/* Generated from COPYING by automake. */
#include "license.h"

/* The suffix that all config files should have. */
#define CONFIG_SUFFIX ".cfg"

#define DEFAULT_DRAW_AXES false
#define DEFAULT_X_MIN -1
#define DEFAULT_X_MAX 1
#define DEFAULT_Y_MIN -1
#define DEFAULT_Y_MAX 1

#define K 0.0000002
#define DEFAULT_HMF_X 0
#define DEFAULT_HMF_Y 0

#define DEFAULT_WIDTH 500
#define DEFAULT_HEIGHT 500

#define DEFAULT_DEFINE_MARGIN_IN_PX 1
#define DEFAULT_MARGIN_M .03
#define DEFAULT_MARGIN_PX 8

#define DEFAULT_DEFINE_ARROW_LEN_MAX_IN_PX 1
#define DEFAULT_ARROW_LEN_MAX_M .07
#define DEFAULT_ARROW_LEN_MAX_PX 20

#define DEFAULT_DEFINE_ARROW_SPACING_IN_PX 1
#define DEFAULT_ARROW_SPACING_M .008
#define DEFAULT_ARROW_SPACING_PX 2

#define DEFAULT_CONDUCTOR_AT_X 0
#define DEFAULT_CONDUCTOR_AT_Y 0
#define DEFAULT_CONDUCTOR_AT \
	(point_t){DEFAULT_CONDUCTOR_AT_X, DEFAULT_CONDUCTOR_AT_Y}
#define DEFAULT_CONDUCTOR_RADIUS .04
#define DEFAULT_CONDUCTOR_CURRENT 1

/* Nifty macros to push and pop to the statusbar. */
#define statusbar_pop() \
	gtk_statusbar_pop(mf->statusbar, mf->context_id)
#define statusbar_push(msg) \
	gtk_statusbar_push(mf->statusbar, mf->context_id, (msg))

/* Pops the previous message and pushes msg. */
#define statusbar_display(msg) { \
	statusbar_pop(); \
	statusbar_push(msg); \
}

/* As display but takes a format string (with arguments). */
#define statusbar_display_fmt(...) { \
	gchar * statusbar_buffer = g_strdup_printf(__VA_ARGS__); \
	statusbar_display(statusbar_buffer); \
	g_free(statusbar_buffer); \
}

typedef struct conductor_s conductor_t;
typedef struct conductor_gtk_s conductor_gtk_t;
typedef struct mfield_s mfield_t;
typedef struct phys_quant_s phys_quant_t;
typedef struct vector_s point_t;
typedef struct prefix_s prefix_t;
typedef struct pxmc_s pxmc_t;
typedef struct vector_s vector_t;
typedef struct vector_widget_s vector_widget_t;

struct vector_s {
	double x;
	double y;
};

struct conductor_s {
	mfield_t * mf;
	conductor_gtk_t * gtk;

	char * name;

	point_t at;
	double radius;
	double current;

	struct conductor_s * next;
};

/* The structure containing the GTK widgets for a conductor. */
struct conductor_gtk_s {
	conductor_t * conductor;
	phys_quant_t * pq_current;
	phys_quant_t * pq_radius;
	vector_widget_t * vw_point;

	GtkWidget * window;
	GtkWidget * hbox;

	bool hidden;
	gint x;
	gint y;
};

/* Common data in the program. */
struct mfield_s {
	/* A mutex that protects all members ins this struct except the GTK+
	 * related ones and the draw thread related stuff. */
	pthread_mutex_t mutex;

	/* The GLFW window that will be used. */
	GLFWwindow * window;

	/* The GL thread will set this to true when it wants the GUI to
	 * initialize the quit mechanisms. This is used to avoid blocking
	 * any threads are dependent on the windowing system as that will
	 * look like a hung-up to the window manager. */
	bool quit;

	/* The filename of the configuration file to save to.
	 * These two variables should only be read and written with an
	 * aquired GTK lock. This way mf locking can be reduced. */
	char * save_filename;
	const char * save_filename_short;

	/* true if there are no unsaved changes (all changes are saved) and
	 * false if there are (not all changes are saved).
	 * The GTK lock should be aquired when writing or reading this
	 * varible. */
	bool saved;

	/* Draw thread stuff */
	pthread_t draw_thread;
	pthread_mutex_t draw_mutex;
	pthread_cond_t draw_cond;
	bool stop;
	bool draw;

	/* GTK stuff */
	GtkWidget * gtk_window;
	GtkWidget * about_dialog;
	GtkWidget * width_spin;
	GtkWidget * height_spin;
	GtkWidget * conductor_vbox;
	GtkWidget * conductor_entry;

	GtkStatusbar * statusbar;
	guint context_id;

	int width;
	int height;
	bool draw_axes;
	double x_min;
	double x_max;
	double y_min;
	double y_max;

	bool define_margin_in_px;
	double margin_m;
	int margin_px;
	pxmc_t * margin_pxmc;

	bool define_arrow_len_max_in_px;
	double arrow_len_max_m;
	int arrow_len_max_px;

	bool define_arrow_spacing_in_px;
	double arrow_spacing_m;
	int arrow_spacing_px;

	/* Vector widgets. */
	vector_widget_t * vector_widgets;

	/* Physical quantities. */
	phys_quant_t * pq_list;

	/* Pixels/meters combos. */
	pxmc_t * pxmc_list;

	/* The homogenous magnetic field. */
	vector_t hmf;

	conductor_t * conductors;
};

/* A vector widget. */
struct vector_widget_s {
	mfield_t * mf;

	/* Set this to true to make the signal handlers return immidiately.
	 */
	bool updating;

	vector_t * vector_p;

	GtkWidget * hbox;
	GtkWidget * x_spin;
	GtkWidget * y_spin;
	GtkWidget * combo;

	struct vector_widget_s * next;
};

/* Physical quantity. */
struct phys_quant_s {
	mfield_t * mf;

	/* Set this to true and the signal handlers connected to the spin
	 * and the combo will do nothing. */
	bool updating;

	double * dest;
	bool nonnegative;

	GtkWidget * hbox;
	GtkWidget * spin;
	GtkWidget * combo;

	int value; /* Value of the spin button. */
	int exponent; /* Exponent shown by the combo box. */

	/* Minimum and maximum value. */
	int min;
	int max;

	/* Links to other physical quantities that need to be either
	 * stricly less or stricly more than this quantity. */
	struct phys_quant_s * less;
	struct phys_quant_s * more;

	struct phys_quant_s * next;
};

/* Short for pixels/meter combo. Used where something can be defined
 * in either pixels or meters. */
struct pxmc_s {
	mfield_t * mf;

	/* Set this to true to make the signal handers return immidiately.
	 */
	bool updating;

	/* The vbox that the 'widget' is packed into. */
	GtkWidget * vbox;

	GtkWidget * px_spin;
	GtkWidget * px_hbox;
	GtkWidget * m_hbox;
	GtkWidget * px_radio;
	GtkWidget * m_radio;

	/* The pq that corresponds to the meters. */
	phys_quant_t * pq;

	/* Pointers to the memory to fill in. */
	bool * define_in_px;
	double * m;
	int * px;

	struct pxmc_s * next;
};

#define PREFIX_END CHAR_MIN
struct prefix_s {
	char character;
	char exponent;
};

/* This list must fulifil these conditions:
 * (1) The exponents must be multiples of 3.
 * (2) The list must contain an entry with exponent = 0.
 * (3) The list must end with exponent = PREFIX_END. */
static prefix_t prefixes[] = {
	{'u', -6},
	{'m', -3},
	{'\0', 0},
	{'k', 3},
	{'M', 6},
	{PREFIX_END, PREFIX_END},
};

static void conductor_gtk_destroy ( conductor_gtk_t * const cg )
	__attribute__((nonnull));
static void conductor_new_with_gtk ( mfield_t * const mf,
									 const char * const name,
									 const point_t at,
									 const double radius,
									 const double current )
	__attribute__((nonnull));
static void conductors_destroy ( mfield_t * const mf )
	__attribute__((nonnull));
static inline double dabs ( const double d );
static inline double len_vector ( const vector_t );
static gboolean cb_delete_conductor ( GtkWidget *, gpointer );
static gboolean cb_cg_window_hide ( GtkWidget *, GdkEvent *, gpointer );
static gboolean cb_cg_window_show ( GtkWidget *, gpointer );
static void * draw_thread_routine ( void * );
static void pq_destroy_all ( mfield_t * const mf )
	__attribute__((nonnull));
static void pq_update ( phys_quant_t * const pq )
	__attribute__((nonnull));
static void pq_update_all ( mfield_t * const mf )
	__attribute__((nonnull));
static void pxmc_destroy_all ( mfield_t * const mf )
	__attribute__((nonnull));
static void pxmc_update ( pxmc_t * const p )
	__attribute__((nonnull));
static void pxmc_update_all ( mfield_t * const mf )
	__attribute__((nonnull));
static bool quit ( mfield_t * const mf ) __attribute__((nonnull));
static void vector_widget_destroy_all ( mfield_t * const mf )
	__attribute__((nonnull));
static void vector_widget_update ( vector_widget_t * const vw )
	__attribute__((nonnull));
static void vector_widget_update_all ( mfield_t * const mf )
	__attribute__((nonnull));

/* Helper function to print errors. */
static void __attribute__((cold, nonnull))
print_error ( const char * const fmt, ... )
{
	fprintf(stderr, "%s: ", PACKAGE_NAME);

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	return;
}

static mfield_t *
mf_new ( void )
{
	mfield_t * const mf = malloc(sizeof(mfield_t));
	if ( mf == NULL ) {
		perror("malloc");
		return NULL;
	}

	if ( pthread_mutex_init(&mf->mutex, NULL) ) {
		perror("pthread_mutex_init");
		free(mf);
		return NULL;
	}

	if ( pthread_mutex_init(&mf->draw_mutex, NULL) ) {
		perror("pthread_mutex_init");
		pthread_mutex_destroy(&mf->mutex);
		free(mf);
		return NULL;
	}

	if ( pthread_cond_init(&mf->draw_cond, NULL) ) {
		perror("pthread_cond_init");
		pthread_mutex_destroy(&mf->draw_mutex);
		pthread_mutex_destroy(&mf->mutex);
		free(mf);
		return NULL;
	}

	mf->quit = false;

	mf->save_filename = NULL;
	mf->save_filename_short = NULL;
	mf->saved = true;

	mf->conductors = NULL;
	mf->vector_widgets = NULL;
	mf->pq_list = NULL;
	mf->pxmc_list = NULL;

	return mf;
}

static void __attribute__((nonnull))
mf_destroy ( mfield_t * const mf )
{
	if ( pthread_cond_destroy(&mf->draw_cond) ) {
		perror("pthread_cond_destroy");
	}
	if ( pthread_mutex_destroy(&mf->draw_mutex) ) {
		perror("pthread_mutex_destroy");
	}
	if ( pthread_mutex_destroy(&mf->mutex) ) {
		perror("pthread_mutex_destroy");
	}

	free(mf->save_filename);
	free(mf);
	return;
}

static void __attribute__((nonnull))
mf_lock ( mfield_t * const mf )
{
	pthread_mutex_lock(&mf->mutex);
}

static void __attribute__((nonnull))
mf_unlock ( mfield_t * const mf )
{
	pthread_mutex_unlock(&mf->mutex);
}

static int __attribute__((nonnull))
mf_start_draw_thread ( mfield_t * const mf )
{
	pthread_attr_t attr;
	if ( pthread_attr_init(&attr) ) {
		perror("pthread_attr_init");
		return 1;
	}

	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	mf_lock(mf);
	mf->draw = false;
	mf->stop = false;
	if ( pthread_create(&mf->draw_thread,
						&attr,
						&draw_thread_routine,
						mf) )
	{
		perror("pthread_create");
		pthread_attr_destroy(&attr);
		mf_unlock(mf);
		return 1;
	}
	mf_unlock(mf);

	pthread_attr_destroy(&attr);
	return 0;
}

static void __attribute__((nonnull))
mf_stop_draw_thread ( mfield_t * const mf )
{
	mf_lock(mf);

	pthread_mutex_lock(&mf->draw_mutex);
	mf->stop = true;
	pthread_cond_signal(&mf->draw_cond);
	pthread_mutex_unlock(&mf->draw_mutex);

	pthread_join(mf->draw_thread, NULL);

	mf_unlock(mf);

	return;
}

static void __attribute__((nonnull))
mf_redraw ( mfield_t * const mf )
{
	pthread_mutex_lock(&mf->draw_mutex);
	mf->draw = true;
	pthread_cond_signal(&mf->draw_cond);
	pthread_mutex_unlock(&mf->draw_mutex);
	return;
}

static int __attribute__((nonnull))
mf_draw_thread_loop ( mfield_t * const mf )
{
	pthread_mutex_lock(&mf->draw_mutex);

again:
	if ( mf->stop ) {
		pthread_mutex_unlock(&mf->draw_mutex);
		return 0;
	}

	if ( mf->draw ) {
		mf->draw = false;
		pthread_mutex_unlock(&mf->draw_mutex);
		return 1;
	}

	pthread_cond_wait(&mf->draw_cond, &mf->draw_mutex);
	goto again;
}

static inline void __attribute__((nonnull))
mf_update ( mfield_t * const mf )
{
	pq_update_all(mf);
	vector_widget_update_all(mf);
	pxmc_update_all(mf);
	mf_redraw(mf);
	return;
}

/* GTK lock. mf unlocked. */
static void __attribute__((nonnull))
mf_load_defaults ( mfield_t * const mf )
{
	free(mf->save_filename);
	mf->save_filename = NULL;
	mf->save_filename_short = NULL;

	mf_lock(mf);

	mf->draw_axes = DEFAULT_DRAW_AXES;
	mf->x_min = DEFAULT_X_MIN;
	mf->x_max = DEFAULT_X_MAX;
	mf->y_min = DEFAULT_Y_MIN;
	mf->y_max = DEFAULT_Y_MAX;

	mf->define_margin_in_px = DEFAULT_DEFINE_MARGIN_IN_PX;
	mf->margin_m = DEFAULT_MARGIN_M;
	mf->margin_px = DEFAULT_MARGIN_PX;

	mf->define_arrow_len_max_in_px = DEFAULT_DEFINE_ARROW_LEN_MAX_IN_PX;
	mf->arrow_len_max_m = DEFAULT_ARROW_LEN_MAX_M;
	mf->arrow_len_max_px = DEFAULT_ARROW_LEN_MAX_PX;

	mf->define_arrow_spacing_in_px = DEFAULT_DEFINE_ARROW_SPACING_IN_PX;
	mf->arrow_spacing_m = DEFAULT_ARROW_SPACING_M;
	mf->arrow_spacing_px = DEFAULT_ARROW_SPACING_PX;

	mf->hmf.x = DEFAULT_HMF_X;
	mf->hmf.y = DEFAULT_HMF_Y;

	mf_unlock(mf);

	/* Destroy conductors first so that pqs and vector widgets are not
	 * updated unnecessarily. */
	conductors_destroy(mf);

	mf_update(mf);

	mf->saved = true;

	return;
}

/* GTK lock.
 * Replaces the current save filename in mf with a copy of filename. It
 * also checks whether filename ends with .cfg and appends it if
 * necessary. */
static int __attribute__((nonnull))
mf_set_save_filename ( mfield_t * const mf,
					   const char * const filename )
{
	free(mf->save_filename);
	mf->save_filename_short = NULL;

	/* Make sure the file ends with .cfg. */
	const char * const suffix = strrchr(filename, '.');
	if ( suffix != NULL && strcmp(suffix, CONFIG_SUFFIX) == 0 ) {
		mf->save_filename = strdup(filename);
		if ( mf->save_filename == NULL ) {
			perror("strdup");
			return 1;
		}
	} else {
		const size_t filename_len = strlen(filename);
		mf->save_filename =
			malloc((filename_len + strlen(CONFIG_SUFFIX) + 1) *
				   sizeof(char));
		if ( mf->save_filename == NULL ) {
			perror("malloc");
			return 1;
		}

		strcpy(mf->save_filename, filename);
		strcpy(mf->save_filename + filename_len, CONFIG_SUFFIX);
	}

	mf->save_filename_short = strrchr(mf->save_filename, '/') + 1;

	return 0;
}

/* GTK lock. mf unlocked.
 * Sets the saved member and updates the statusbar. */
static inline void __attribute__((nonnull))
mf_modified ( mfield_t * const mf )
{
	mf->saved = false;
	if ( mf->save_filename_short != NULL ) {
		statusbar_display_fmt("%s modified.", mf->save_filename_short);
	} else {
		statusbar_display("New configuration modified.");
	}
	return;
}

static inline void __attribute__((nonnull))
load_double ( const config_setting_t * const setting,
			  const char * const name,
			  double * const dest,
			  const double default_value )
{
	if ( !config_setting_lookup_float(setting, name, dest) ) {
		*dest = default_value;
	}
	return;
}

static inline void __attribute__((nonnull))
load_bool ( const config_setting_t * const setting,
			const char * const name,
			bool * const dest,
			const bool default_value )
{
	int pre;
	if ( !config_setting_lookup_bool(setting, name, &pre) ) {
		*dest = default_value;
	}
	if ( pre ) *dest = true;
	else *dest = false;

	return;
}

static inline void __attribute__((nonnull))
load_int ( const config_setting_t * const setting,
		   const char * const name,
		   int * const dest,
		   const int default_value )
{
	if ( !config_setting_lookup_int(setting, name, dest) ) {
		*dest = default_value;
	}
	return;
}

/* GTK lock. mf unlocked.
 * Parses the file at filename loads the values to the mf.
 * Returns true on success and false on failure. */
static bool __attribute__((nonnull))
mf_open ( mfield_t * const mf, const char * const filename )
{
	config_t save;
	config_init(&save);

	if ( mf_set_save_filename(mf, filename) ) {
		goto error;
	}

	if ( !config_read_file(&save, mf->save_filename) ) {
		print_error("Error reading save file.\n");
		fprintf(stderr, "%s:%d - %s\n", config_error_file(&save),
				config_error_line(&save), config_error_text(&save));
		goto error;
	}

	config_setting_t * const root = config_root_setting(&save);

	mf_lock(mf);

/* Load double from root. */
#define LDR(x,d) load_double(root, #x, &mf->x, d)
/* Load int from root. */
#define LIR(x,d) load_int(root, #x, &mf->x, d)
/* Load bool from root. */
#define LBR(x,d) load_bool(root, #x, &mf->x, d)
	LIR(width, DEFAULT_WIDTH);
	LIR(height, DEFAULT_HEIGHT);
	LBR(draw_axes, DEFAULT_DRAW_AXES);
	LDR(x_min, DEFAULT_X_MIN);
	LDR(x_max, DEFAULT_X_MAX);
	LDR(y_min, DEFAULT_Y_MIN);
	LDR(y_max, DEFAULT_Y_MAX);

	LBR(define_margin_in_px, DEFAULT_DEFINE_MARGIN_IN_PX);
	LDR(margin_m, DEFAULT_MARGIN_M);
	LIR(margin_px, DEFAULT_MARGIN_PX);

	LBR(define_arrow_len_max_in_px, DEFAULT_DEFINE_ARROW_LEN_MAX_IN_PX);
	LDR(arrow_len_max_m, DEFAULT_ARROW_LEN_MAX_M);
	LIR(arrow_len_max_px, DEFAULT_ARROW_LEN_MAX_PX);

	LBR(define_arrow_spacing_in_px, DEFAULT_DEFINE_ARROW_SPACING_IN_PX);
	LDR(arrow_spacing_m, DEFAULT_ARROW_SPACING_M);
	LIR(arrow_spacing_px, DEFAULT_ARROW_SPACING_PX);
#undef LBE
#undef LDR
#undef LIR

	config_setting_t * setting;
	setting = config_lookup(&save, "hmf");
	if ( setting != NULL ) {
		load_double(setting, "x", &mf->hmf.x, DEFAULT_HMF_X);
		load_double(setting, "y", &mf->hmf.y, DEFAULT_HMF_Y);
	} else {
		mf->hmf.x = DEFAULT_HMF_X;
		mf->hmf.y = DEFAULT_HMF_Y;
	}

	setting = config_lookup(&save, "conductors");
	if ( setting != NULL ) {
		int count = config_setting_length(setting);
		while ( --count >= 0 ) {
			config_setting_t * const s =
				config_setting_get_elem(setting, count);
			assert(s != NULL);

			const char * name;
			if ( !config_setting_lookup_string(s, "name", &name) ) {
				print_error("Conductor setting %d lacks a name.\n",
							count);
				continue;
			}

			point_t at;
			double radius;
			double current;
			load_double(s, "at_x", &at.x, DEFAULT_CONDUCTOR_AT_X);
			load_double(s, "at_y", &at.y, DEFAULT_CONDUCTOR_AT_Y);
			load_double(s, "radius", &radius, DEFAULT_CONDUCTOR_RADIUS);
			load_double(s, "current", &current,
						DEFAULT_CONDUCTOR_CURRENT);

			/* The mf needs to be unlocked when conductors are added. */
			mf_unlock(mf);
			conductor_new_with_gtk(mf, name, at, radius, current);
			mf_lock(mf);
		}
	} else {
		mf->conductors = NULL;
	}

	mf_unlock(mf);
	mf->saved = true;

	config_destroy(&save);

	mf_update(mf);

	return true;
error:
	mf_unlock(mf);
	config_destroy(&save);
	return false;
}

static inline void __attribute__((nonnull))
save_double ( config_setting_t * const parent,
			  const char * const name,
			  const double value )
{
	config_setting_t * setting;
	setting = config_setting_add(parent, name, CONFIG_TYPE_FLOAT);
	assert(setting != NULL);
	config_setting_set_float(setting, value);
	return;
}

static inline void __attribute__((nonnull))
save_bool ( config_setting_t * const parent,
			const char * const name,
			const bool value )
{
	/* Let's don't make any assumptions about how bool is defined. */
	const int i = value ? CONFIG_TRUE : CONFIG_FALSE;

	config_setting_t * setting;
	setting = config_setting_add(parent, name, CONFIG_TYPE_BOOL);
	assert(setting != NULL);
	config_setting_set_bool(setting, i);
	return;
}


static inline void __attribute__((nonnull))
save_int ( config_setting_t * const parent,
		   const char * const name,
		   const int value )
{
	config_setting_t * setting;
	setting = config_setting_add(parent, name, CONFIG_TYPE_INT);
	assert(setting != NULL);
	config_setting_set_int(setting, value);
	return;
}

static inline void __attribute__((nonnull))
save_string ( config_setting_t * const parent,
			  const char * const name,
			  const char * const string )
{
	config_setting_t * setting;
	setting = config_setting_add(parent, name, CONFIG_TYPE_STRING);
	assert(setting != NULL);
	config_setting_set_string(setting, string);
	return;
}

/* GTK lock. mf unlocked.
 * Saves the mf to the file specified by filename.
 * If filename is NULL the mf is saved to the current file, that is
 * mf->save_filename.
 * Returns true on success and false on failure. */
static bool __attribute__((nonnull (1) ))
mf_save ( mfield_t * const mf, const char * const filename )
{
	if ( filename != NULL ) {
		if ( mf_set_save_filename(mf, filename) ) {
			goto error;
		}
	} else if ( filename == NULL && mf->save_filename == NULL ) {
		goto error;
	}

	config_t save;
	config_init(&save);

	config_setting_t * const root = config_root_setting(&save);

	mf_lock(mf);

/* Save bool to root. */
#define SBR(x) save_bool(root, #x, mf->x)
/* Save double to root. */
#define SDR(x) save_double(root, #x, mf->x)
/* Save int to root. */
#define SIR(x) save_int(root, #x, mf->x)
	SIR(width);
	SIR(height);
	SBR(draw_axes);
	SDR(x_min);
	SDR(x_max);
	SDR(y_min);
	SDR(y_max);

	SBR(define_margin_in_px);
	SDR(margin_m);
	SIR(margin_px);

	SBR(define_arrow_len_max_in_px);
	SDR(arrow_len_max_m);
	SIR(arrow_len_max_px);

	SBR(define_arrow_spacing_in_px);
	SDR(arrow_spacing_m);
	SIR(arrow_spacing_px);
#undef SDR
#undef SIR
#undef SBR

	config_setting_t * setting;
	setting = config_setting_add(root, "hmf", CONFIG_TYPE_GROUP);
	save_double(setting, "x", mf->hmf.x);
	save_double(setting, "y", mf->hmf.y);

	setting = config_setting_add(root, "conductors", CONFIG_TYPE_LIST);
	for ( conductor_t * c = mf->conductors; c != NULL; c = c->next ) {
		config_setting_t * const s =
			config_setting_add(setting, NULL, CONFIG_TYPE_GROUP); 
		assert(s != NULL);

		save_string(s, "name", c->name);
		save_double(s, "at_x", c->at.x);
		save_double(s, "at_y", c->at.y);
		save_double(s, "radius", c->radius);
		save_double(s, "current", c->current);
	}

	if ( !config_write_file(&save, mf->save_filename) ) {
		print_error("Error while writing save file.\n");
		goto error;
	}

	mf_unlock(mf);
	mf->saved = true;

	config_destroy(&save);

	return true;
error:
	mf_unlock(mf);
	return false;
}

static void
pq_update_minmax ( phys_quant_t * const pq )
{
	/* To aid understanding the code each situation has been provided
	 * with an example. */
	if ( pq->less != NULL ) {
		phys_quant_t * const pq1 = pq->less;
		if ( pq->exponent > pq1->exponent ) {
			/* Sign: |-|-|-|-|- |- |- | +- |+ |+ |+ |+|+|+|+|
			 * Exp:  |3|2|1|0|-1|-2|-3|-inf|-3|-2|-1|0|1|2|3|
			 * pq:   | |X|X|X|  |  |  |XXXX|  |  |  |X|X|X| |
			 * pq1:  | | | | |XX|XX|XX|XXXX|XX|XX|XX| | | | | */
			if ( pq->value < 0 ) {
				/* Impossible case. Adjust value. */
				if ( pq1->value >= 0 ) {
					pq->value = 1;
				} else /* ( pq1->value < 0 ) */ {
					pq->value = 0;
				}
				gtk_spin_button_set_value(GTK_SPIN_BUTTON(pq->spin),
										  pq->value);
			}

			if ( pq->value == 0 ) {
				pq->min = 0;
				pq1->max = -1;
			} else /* ( pq->value > 0 ) */ {
				if ( pq1->value >= 0 ) {
					pq->min = 1;
				} else /* ( pq1->value < 0 ) */ {
					pq->min = 0;
				}
				pq1->max = 999;
			}
		} else if ( pq->exponent < pq1->exponent ) {
			/* Sign: |-|-|-|-|- |- |- | +- |+ |+ |+ |+|+|+|+|
			 * Exp:  |3|2|1|0|-1|-2|-3|-inf|-3|-2|-1|0|1|2|3|
			 * pq:   | | | | |XX|XX|XX|XXXX|XX|XX|XX| | | | |
			 * pq1:  | |X|X|X|  |  |  |XXXX|  |  |  |X|X|X| | */
			assert(pq1->value <= 0);
			if ( pq1->value == 0 ) {
				pq->min = 1;
			} else /* ( pq1->value < 0 ) */ {
				if ( pq->nonnegative ) {
					pq->min = 0;
				} else {
					pq->min = -999;
				}
			}
			if ( pq->value > 0 ) {
				pq1->max = 0;
			} else /* ( pq->value <= 0 ) */ {
				pq1->max = -1;
			}
		} else /* ( pq->exponent == pq1->exponent ) */ {
			/* Sign: |-|-|-|-|- |- |- | +- |+ |+ |+ |+|+|+|+|
			 * Exp:  |3|2|1|0|-1|-2|-3|-inf|-3|-2|-1|0|1|2|3|
			 * pq:   | | | | |XX|XX|XX|XXXX|XX|XX|XX| | | | |
			 * pq1:  | | | | |XX|XX|XX|XXXX|XX|XX|XX| | | | | */
			pq->min = pq1->value + 1;
			pq1->max = pq->value - 1;
		}
		gtk_spin_button_set_range(GTK_SPIN_BUTTON(pq->spin),
								  pq->min, pq->max);
		gtk_spin_button_set_range(GTK_SPIN_BUTTON(pq1->spin),
								  pq1->min, pq1->max);
	}

	if ( pq->more != NULL ) {
		phys_quant_t * const pq1 = pq->more;
		if ( pq->exponent > pq1->exponent ) {
			/* Sign: |-|-|-|-|- |- |- | +- |+ |+ |+ |+|+|+|+|
			 * Exp:  |3|2|1|0|-1|-2|-3|-inf|-3|-2|-1|0|1|2|3|
			 * pq:   | |X|X|X|  |  |  |XXXX|  |  |  |X|X|X| |
			 * pq1:  | | | | |XX|XX|XX|XXXX|XX|XX|XX| | | | | */
			if ( pq->value >= 0 ) {
				/* Impossible case. Adjust value. */
				if ( pq1->value <= 0 ) {
					pq->value = -1;
				} else /* ( pq1->value > 0 ) */ {
					pq->value = 0;
				}
				gtk_spin_button_set_value(GTK_SPIN_BUTTON(pq->spin),
					pq->value);
			}

			if ( pq->value == 0 ) {
				pq1->min = 1;
			} else /* ( pq->value < 0 ) */ {
				if ( pq1->nonnegative ) {
					pq1->min = 0;
				} else {
					pq1->min = -999;
				}
			}
			if ( pq1->value <= 0 ) {
				pq->max = -1;
			} else /* ( pq1->value > 0 ) */ {
				pq->max = 0;
			} 
		} else if ( pq->exponent < pq1->exponent ) {
			/* Sign: |-|-|-|-|- |- |- | +- |+ |+ |+ |+|+|+|+|
			 * Exp:  |3|2|1|0|-1|-2|-3|-inf|-3|-2|-1|0|1|2|3|
			 * pq:   | | | | |XX|XX|XX|XXXX|XX|XX|XX| | | | |
			 * pq1:  | |X|X|X|  |  |  |XXXX|  |  |  |X|X|X| | */
			assert(pq1->value >= 0);
			if ( pq->value < 0 ) {
				pq1->min = 0;
			} else /* ( pq->value >= 0 ) */ {
				pq1->min = 1;
			}
			if ( pq1->value == 0 ) {
				pq->max = -1;
			} else /* ( pq1->value > 0 ) */ {
				pq->max = 999;
			}
		} else /* ( pq->exponent == pq1->exponent ) */ {
			/* Sign: |-|-|-|-|- |- |- | +- |+ |+ |+ |+|+|+|+|
			 * Exp:  |3|2|1|0|-1|-2|-3|-inf|-3|-2|-1|0|1|2|3|
			 * pq:   | | | | |XX|XX|XX|XXXX|XX|XX|XX| | | | |
			 * pq1:  | | | | |XX|XX|XX|XXXX|XX|XX|XX| | | | | */
			pq->max = pq1->value - 1;
			pq1->min = pq->value + 1;
		}
		gtk_spin_button_set_range(GTK_SPIN_BUTTON(pq->spin),
								  pq->min, pq->max);
		gtk_spin_button_set_range(GTK_SPIN_BUTTON(pq1->spin),
								  pq1->min, pq1->max);
	}

	return;
}

static gboolean __attribute__((nonnull))
pq_changed ( GtkWidget * widget, gpointer data )
{
	phys_quant_t * const pq = (phys_quant_t *)data;
	if ( pq->updating ) return TRUE;

	pq->value = gtk_spin_button_get_value(GTK_SPIN_BUTTON(pq->spin));

	const int i = gtk_combo_box_get_active(GTK_COMBO_BOX(pq->combo));
	pq->exponent = prefixes[i].exponent;

	pq_update_minmax(pq);

	const double new_value = pq->value * pow(10, pq->exponent);
	const bool changed = (*pq->dest != new_value);
	mf_lock(pq->mf);
	if ( changed ) {
		*pq->dest = new_value;
	}
	mf_unlock(pq->mf);

	if ( changed ) {
		mf_modified(pq->mf);
	}

	mf_redraw(pq->mf);

	return TRUE;
}

static phys_quant_t * __attribute__((nonnull))
pq_new ( mfield_t * const mf,
		 double * const dest,
		 const char * const name,
		 const char * const unit )
{
	phys_quant_t * const pq = malloc(sizeof(phys_quant_t));
	if ( pq == NULL ) {
		perror("malloc");
		return NULL;
	}

	/* Buffer for the prefixed units. */
	char * buffer = malloc(strlen(unit)+2);
	if ( buffer == NULL ) {
		perror("malloc");
		free(pq);
		return NULL;
	}

	pq->mf = mf;
	pq->updating = false;
	pq->dest = dest;
	pq->nonnegative = false;
	pq->min = -999;
	pq->max = 999;
	pq->less = NULL;
	pq->more = NULL;

	pq->hbox = gtk_hbox_new(TRUE, 5);

	if ( strcmp(name, "") != 0 ) {
		GtkWidget * label = gtk_label_new(name);
		gtk_box_pack_start_defaults(GTK_BOX(pq->hbox), label);
	}

	pq->spin = gtk_spin_button_new_with_range(pq->min, pq->max, 1);
	pq->combo = gtk_combo_box_text_new();
	for ( int i = 0; prefixes[i].exponent != PREFIX_END; ++i ) {
		buffer[0] = prefixes[i].character;
		buffer[1] = '\0';
		strcat(buffer, unit);
		gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(pq->combo),
									   buffer);
	}
	free(buffer);

	/* Display the correct values. */
	pq_update(pq);

	g_signal_connect(pq->spin, "value-changed",
					 G_CALLBACK(pq_changed), pq);
	g_signal_connect(pq->combo, "changed", G_CALLBACK(pq_changed), pq);

	gtk_box_pack_start_defaults(GTK_BOX(pq->hbox), pq->spin);
	gtk_box_pack_start_defaults(GTK_BOX(pq->hbox), pq->combo);

	/* Link it into the list. */
	mf_lock(mf);
	pq->next = mf->pq_list;
	mf->pq_list = pq;
	mf_unlock(mf);

	return pq;
}

static void
pq_destroy ( phys_quant_t * const pq )
{
	if ( pq == NULL ) return;

	/* Unlink */
	mfield_t * const mf = pq->mf;
	if ( pq == mf->pq_list ) {
		mf->pq_list = pq->next;
	} else {
		phys_quant_t * prev = mf->pq_list;
		while ( prev->next != pq && prev != NULL ) {
			prev = prev->next;
		}
		assert(prev != NULL);
		prev->next = pq->next;
	}

	free(pq);

	return;
}

static void __attribute__((nonnull))
pq_destroy_all ( mfield_t * const mf )
{
	while ( mf->pq_list != NULL ) {
		pq_destroy(mf->pq_list);
	}
	return;
}

static void __attribute__((nonnull))
pq_nonnegative ( phys_quant_t * const pq )
{
	pq->nonnegative = true;
	if ( pq->min < 0 ) {
		pq->min = 0;
		gtk_spin_button_set_range(GTK_SPIN_BUTTON(pq->spin),
								  pq->min, pq->max);
	}
	return;
}

static void __attribute__((nonnull))
pq_set_less ( phys_quant_t * const pq, phys_quant_t * const less )
{
	assert(pq->value*pow(10, pq->exponent) >
		  less->value*pow(10, less->exponent));
	pq->less = less;
	less->more = pq;
	pq_update_minmax(pq);
	return;
}

static void __attribute__((nonnull))
pq_update ( phys_quant_t * const pq )
{
	/* Prevents the signal handlers from changing values. */
	pq->updating = true;

	int i = 0;
	if ( *pq->dest == 0 ) {
		while ( prefixes[i].exponent != 0 ) ++i;
	} else {
		const double avalue = dabs(*pq->dest);
		while ( prefixes[i].exponent != PREFIX_END ) {
			if ( pow(10, prefixes[i].exponent) * 1000 > avalue ) {
				break;
			}
			++i;
		}

		/* The prefix should exist. */
		assert(prefixes[i].exponent != PREFIX_END);
	}
	pq->exponent = prefixes[i].exponent;
	pq->value = *pq->dest / pow(10, pq->exponent);

	gtk_spin_button_set_value(GTK_SPIN_BUTTON(pq->spin), pq->value);
	gtk_combo_box_set_active(GTK_COMBO_BOX(pq->combo), i);

	/* Let the signal handlers do their thing again. */
	pq->updating = false;
	return;
}

static void __attribute__((nonnull))
pq_update_all ( mfield_t * const mf )
{
	for ( phys_quant_t * pq = mf->pq_list; pq != NULL; pq = pq->next ) {
		pq_update(pq);
	}
	return;
}

static gboolean
pxmc_px_spin_changed ( GtkSpinButton * spin, gpointer data )
{
	pxmc_t * const p = (pxmc_t *)data;

	const int new_px =
		gtk_spin_button_get_value(GTK_SPIN_BUTTON(p->px_spin));
	const bool changed = (*p->px != new_px);
	mf_lock(p->mf);
	if ( changed ) {
		*p->px = new_px;
	}
	mf_unlock(p->mf);

	if ( changed ) {
		mf_modified(p->mf);
	}

	mf_redraw(p->mf);

	return FALSE;
}

static void __attribute__((nonnull))
pxmc_hide_and_show ( pxmc_t * const p )
{
	if ( *p->define_in_px ) {
		gtk_widget_hide(p->m_hbox);
		gtk_widget_show(p->px_hbox);
	} else {
		gtk_widget_hide(p->px_hbox);
		gtk_widget_show(p->m_hbox);
	}
	return;
}

static void __attribute__((nonnull))
pxmc_hide_and_show_all ( mfield_t * const mf )
{
	for ( pxmc_t * p = mf->pxmc_list; p != NULL; p = p->next ) {
		pxmc_hide_and_show(p);
	}
	return;
}

static gboolean
pxmc_radio_toggled ( GtkToggleButton * toggle, gpointer data )
{
	pxmc_t * const p = (pxmc_t *)data;
	if ( p->updating ) return TRUE;

	mf_lock(p->mf);
	*p->define_in_px = gtk_toggle_button_get_active(
						GTK_TOGGLE_BUTTON(p->px_radio));
	mf_unlock(p->mf);
	mf_redraw(p->mf);

	/* Read the variable and set the sensitivity here to reduce the
	 * locking time. It is OK to do it here because the GL thread will
	 * only read from the variable and because no other function can
	 * change it. This function has already aquired the gtk lock. */
	pxmc_hide_and_show(p);
	return TRUE;
}

static pxmc_t * __attribute__((nonnull))
pxmc_new ( mfield_t * const mf,
		   bool * const define_in_px,
		   double * const m,
		   int * const px,
		   const char * const name )
{
	pxmc_t * const p = malloc(sizeof(pxmc_t));
	if ( p == NULL ) {
		perror("malloc");
		return NULL;
	}

	p->mf = mf;
	p->updating = false;
	p->define_in_px = define_in_px;
	p->m = m;
	p->px = px;

	p->pq = pq_new(mf, p->m, "", "m");
	if ( p->pq == NULL ) {
		free(p);
		return NULL;
	}
	pq_nonnegative(p->pq);

	p->vbox = gtk_vbox_new(FALSE, 5);
	gtk_container_set_border_width(GTK_CONTAINER(p->vbox), 3);

	GtkWidget * label = gtk_label_new(name);
	gtk_box_pack_start_defaults(GTK_BOX(p->vbox), label);

	/* HBox for the radio buttons. */
	GtkWidget * hbox = gtk_hbox_new(TRUE, 5);
	gtk_box_pack_start_defaults(GTK_BOX(p->vbox), hbox);

	p->px_radio = gtk_radio_button_new_with_label(NULL, "Define in px");
	p->m_radio = gtk_radio_button_new_with_label_from_widget(
				GTK_RADIO_BUTTON(p->px_radio), "Define in m");
	g_signal_connect(p->px_radio, "toggled",
					 G_CALLBACK(pxmc_radio_toggled), p);
	g_signal_connect(p->m_radio, "toggled",
					 G_CALLBACK(pxmc_radio_toggled), p);
	gtk_box_pack_start_defaults(GTK_BOX(hbox), p->px_radio);
	gtk_box_pack_start_defaults(GTK_BOX(hbox), p->m_radio);

	p->px_hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start_defaults(GTK_BOX(p->vbox), p->px_hbox);

	p->px_spin = gtk_spin_button_new_with_range(0, 100, 1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(p->px_spin), *p->px);
	g_signal_connect(p->px_spin, "value-changed",
					 G_CALLBACK(pxmc_px_spin_changed), p);
	gtk_box_pack_start_defaults(GTK_BOX(p->px_hbox), p->px_spin);

	label = gtk_label_new("px");
	gtk_box_pack_start_defaults(GTK_BOX(p->px_hbox), label);

	p->m_hbox = p->pq->hbox;
	gtk_box_pack_start_defaults(GTK_BOX(p->vbox), p->m_hbox);

	pxmc_update(p);

	/* Link it. */
	mf_lock(mf);
	p->next = mf->pxmc_list;
	mf->pxmc_list = p;
	mf_unlock(mf);

	return p;
}

static void
pxmc_destroy ( pxmc_t * const p )
{
	if ( p == NULL ) return;

	/* Unlink */
	mfield_t * const mf = p->mf;
	if ( p == mf->pxmc_list ) {
		mf->pxmc_list = p->next;
	} else {
		pxmc_t * prev = mf->pxmc_list;
		while ( prev->next != p && prev != NULL ) {
			prev = prev->next;
		}
		assert(prev != NULL);
		prev->next = p->next;
	}

	pq_destroy(p->pq);
	free(p);
	return;
}

static void __attribute__((nonnull))
pxmc_destroy_all ( mfield_t * const mf )
{
	while ( mf->pxmc_list != NULL ) {
		pxmc_destroy(mf->pxmc_list);
	}
}

static void __attribute__((nonnull))
pxmc_set_px_max ( pxmc_t * const p, const unsigned int max )
{
	gtk_spin_button_set_range(GTK_SPIN_BUTTON(p->px_spin), 0, max);
	return;
}

static void __attribute__((nonnull))
pxmc_update ( pxmc_t * const p )
{
	/* Block the signal handlers from messing with the data. */
	p->updating = true;

	if ( *p->define_in_px ) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(p->px_radio),
									 TRUE);
	} else {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(p->m_radio),
									 TRUE);
	}
	pxmc_hide_and_show(p);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(p->px_spin), *p->px);
	pq_update(p->pq);

	/* Let the signal handlers do update values again. */
	p->updating = false;
	return;
}

static void __attribute__((nonnull))
pxmc_update_all ( mfield_t * const mf )
{
	for ( pxmc_t * p = mf->pxmc_list; p != NULL; p = p->next ) {
		pxmc_update(p);
	}
	return;
}

static gboolean __attribute__((nonnull))
vw_changed ( GtkWidget * widget, gpointer data )
{
	vector_widget_t * const vw = (vector_widget_t *)data;
	if ( vw->updating ) return TRUE;

	const double x_value =
		gtk_spin_button_get_value(GTK_SPIN_BUTTON(vw->x_spin));
	const double y_value =
		gtk_spin_button_get_value(GTK_SPIN_BUTTON(vw->y_spin));

	const int i = gtk_combo_box_get_active(GTK_COMBO_BOX(vw->combo));
	const int exponent = prefixes[i].exponent;

	const double new_x = x_value * pow(10, exponent);
	const double new_y = y_value * pow(10, exponent);
	const bool changed = (vw->vector_p->x != new_x ||
						  vw->vector_p->y != new_y);
	mf_lock(vw->mf);
	if ( changed ) {
		vw->vector_p->x = new_x;
		vw->vector_p->y = new_y;
	}
	mf_unlock(vw->mf);

	if ( changed ) {
		mf_modified(vw->mf);
	}

	mf_redraw(vw->mf);

	return TRUE;
}

static vector_widget_t * __attribute__((nonnull))
vector_widget_new ( mfield_t * const mf,
					vector_t * const vector_p,
					const char * const name,
					const char * const unit )
{
	vector_widget_t * const vw = malloc(sizeof(vector_widget_t));
	if ( vw == NULL ) {
		perror("malloc");
		return NULL;
	}

	/* Buffer for the prefixed units. */
	char * const buffer = malloc(strlen(unit)+2);
	if ( buffer == NULL ) {
		perror("malloc");
		free(vw);
		return NULL;
	}

	vw->mf = mf;
	vw->updating = false;
	vw->vector_p = vector_p;

	vw->hbox = gtk_hbox_new(TRUE, 5);

	GtkWidget * label = gtk_label_new(name);
	gtk_box_pack_start_defaults(GTK_BOX(vw->hbox), label);

	GtkWidget * vbox = gtk_vbox_new(FALSE, 2);
	gtk_box_pack_start_defaults(GTK_BOX(vw->hbox), vbox);

	vw->x_spin = gtk_spin_button_new_with_range(-999, 999, 1);
	vw->y_spin = gtk_spin_button_new_with_range(-999, 999, 1);
	gtk_spin_button_set_digits(GTK_SPIN_BUTTON(vw->x_spin), 2);
	gtk_spin_button_set_digits(GTK_SPIN_BUTTON(vw->y_spin), 2);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), vw->x_spin);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), vw->y_spin);

	vw->combo = gtk_combo_box_text_new();
	gtk_box_pack_start_defaults(GTK_BOX(vw->hbox), vw->combo);

	for ( int i = 0; prefixes[i].exponent != PREFIX_END; ++i ) {
		buffer[0] = prefixes[i].character;
		buffer[1] = '\0';
		strcat(buffer, unit);
		gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(vw->combo),
									   buffer);
	}
	free(buffer);

	/* Make sure the correct values are displayed. */
	vector_widget_update(vw);

	g_signal_connect(vw->x_spin, "value-changed",
					 G_CALLBACK(vw_changed), vw);
	g_signal_connect(vw->y_spin, "value-changed",
					 G_CALLBACK(vw_changed), vw);
	g_signal_connect(vw->combo, "changed", G_CALLBACK(vw_changed), vw);

	/* Link it into the list. */
	mf_lock(mf);
	vw->next = mf->vector_widgets;
	mf->vector_widgets = vw;
	mf_unlock(mf);

	return vw;
}

static void
vector_widget_destroy ( vector_widget_t * const vw )
{
	if ( vw == NULL ) return;

	/* Unlink */
	mfield_t * const mf = vw->mf;
	if ( vw == mf->vector_widgets ) {
		mf->vector_widgets = vw->next;
	} else {
		vector_widget_t * prev = mf->vector_widgets;
		while ( prev->next != vw && prev != NULL ) {
			prev = prev->next;
		}
		assert(prev != NULL);
		prev->next = vw->next;
	}

	free(vw);

	return;
}

static void __attribute__((nonnull))
vector_widget_destroy_all ( mfield_t * const mf )
{
	while ( mf->vector_widgets != NULL ) {
		vector_widget_destroy(mf->vector_widgets);
	}
	return;
}

static void __attribute__((nonnull))
vector_widget_update ( vector_widget_t * const vw )
{
	/* Make sure the signal handlers do nothing. */
	vw->updating = true;

	int i = 0;
	if ( vw->vector_p->x == 0 ) {
		while ( prefixes[i].exponent != 0 ) ++i;
	} else {
		const double avalue = dabs(vw->vector_p->x);
		while ( prefixes[i].exponent != PREFIX_END ) {
			if ( pow(10, prefixes[i].exponent) * 1000 > avalue ) {
				break;
			}
			++i;
		}

		/* The prefix should exist. */
		assert(prefixes[i].exponent != PREFIX_END);
	}

	const int exponent = prefixes[i].exponent;
	const double x_value = vw->vector_p->x / pow(10, exponent);
	const double y_value = vw->vector_p->y / pow(10, exponent);

	gtk_spin_button_set_value(GTK_SPIN_BUTTON(vw->x_spin), x_value);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(vw->y_spin), y_value);
	gtk_combo_box_set_active(GTK_COMBO_BOX(vw->combo), i);

	/* Let the callbacks do something again. */
	vw->updating = false;
	return;
}

static void __attribute__((nonnull))
vector_widget_update_all ( mfield_t * const mf )
{
	for ( vector_widget_t * vw = mf->vector_widgets;
		  vw != NULL;
		  vw = vw->next )
	{
		vector_widget_update(vw);
	}
	return;
}

static inline double
dabs ( const double d )
{
	if ( d > 0 ) {
		return d;
	} else {
		return -d;
	}
}

static bool __attribute__((const))
line_segment_collides_with_circle ( const point_t p1,
									const point_t p2,
									const point_t centre,
									const double radius )
{
	/* To find out how these formulas were calculated see
	 * doc/line_segment_circle.{odt,pdf}. */
	vector_t p1c;
	p1c.x = centre.x - p1.x;
	p1c.y = centre.y - p1.y;

	vector_t p1p2;
	p1p2.x = p2.x - p1.x;
	p1p2.y = p2.y - p1.y;

	const double a = p1c.x * p1p2.y - p1c.y * p1p2.x;
	if ( a*a <= radius*radius * (p1p2.x*p1p2.x + p1p2.y*p1p2.y) )
	{
		if ( p1c.x * p1p2.x + p1c.y * p1p2.y >= 0 )
		{
			if ( (centre.x - p2.x) * p1p2.x +
					(centre.y - p2.y) * p1p2.y <= 0 ||
				 (centre.x - p2.x) * (centre.x - p2.x) +
					(centre.y - p2.y) * (centre.y - p2.y) <=
					radius*radius )
			{
				return true;
			}
		} else if ( p1c.x * p1c.x + p1c.y * p1c.y <= radius * radius ) {
			return true;
		}
	}

	return false;
}

static inline void
draw_arrow ( const point_t s, const point_t e )
{
	glBegin(GL_LINES);
	glVertex2f(s.x, s.y);
	glVertex2f(e.x, e.y);
	glEnd();

	vector_t v;
	v.x = e.x - s.x;
	v.y = e.y - s.y;

	/* A percentage of the arrow where the triangle will start. */
	const double k = .36;
	const double l = .39; /* Length of an arrow "tip". */
	const double v_len = len_vector(v);

	point_t b1; /* Buffer point on the line. */
	b1.x = s.x + (1-k)*v.x;
	b1.y = s.y + (1-k)*v.y;

	const double factor = sqrt(l*l / v_len*v_len - k*k);

	point_t p1, p2;
	p1.x = b1.x + v.y * factor;
	p1.y = b1.y - v.x * factor;
	p2.x = b1.x - v.y * factor;
	p2.y = b1.y + v.x * factor;

	glBegin(GL_TRIANGLES);
	glVertex2f(e.x, e.y);
	glVertex2f(p1.x, p1.y);
	glVertex2f(p2.x, p2.y);
	glEnd();

	return;
}

/* Draws a conductor. The mfield the conductor belongs to needs to be
 * locked when this function is called. */
static inline void
draw_conductor ( const conductor_t * const conductor )
{
	const int sides = 40;

	const double x = conductor->at.x;
	const double y = conductor->at.y;
	const double r = conductor->radius;

	glColor4i(0, 0, 0, INT_MAX);
	glBegin(GL_LINES);
	for ( int a = 0; a < 360; ++a ) {
		const double radian = a * M_PI / 180;
		glVertex2f(x, y);
		glVertex2f(x + r * sin(radian), y + r * cos(radian));
	}
	glEnd();

	glColor4i(INT_MAX, INT_MAX, INT_MAX, INT_MAX);

	glBegin(GL_LINE_LOOP);
	for ( int a = 0; a <= 360; a += 360 / sides ) {
		const double radian = a * M_PI / 180;
		glVertex2f(x + r * cos(radian), y + r * sin(radian));
	}
	glEnd();

	if ( conductor->current > 0 ) {
		/* Current goes into the screen. */
		const double x1 = x - r * M_SQRT1_2;
		const double x2 = x + r * M_SQRT1_2;
		const double y1 = y - r * M_SQRT1_2;
		const double y2 = y + r * M_SQRT1_2;

		glBegin(GL_LINES);
		glVertex2f(x1, y1);
		glVertex2f(x2, y2);
		glVertex2f(x1, y2);
		glVertex2f(x2, y1);
		glEnd();
	} else if ( conductor->current < 0 ) {
		/* Current goes out of the screen. */
		const double iradius = r / 3;
		glBegin(GL_LINES);
		for ( int a = 0; a <= 360; ++a ) {
			const double radian = a * M_PI / 180;
			glVertex2f(x, y);
			glVertex2f(x + iradius * cos(radian),
					   y + iradius * sin(radian));
		}
		glEnd();
	}

	return;
}

static inline void
draw_conductors ( mfield_t * const mf )
{
	mf_lock(mf);
	for ( const conductor_t * conductor = mf->conductors;
		  conductor != NULL;
		  conductor = conductor->next )
	{
		draw_conductor(conductor);
	}
	mf_unlock(mf);
	return;
}

static inline double __attribute__((const))
len_vector ( const vector_t v )
{
	return sqrt(v.x*v.x + v.y*v.y);
}

static bool
point_in_conductor ( const mfield_t * const mfield,
					 const point_t point )
{
	for ( conductor_t * c = mfield->conductors; c != NULL; c = c->next )
	{
		if ( pow(c->at.x - point.x, 2) + pow(c->at.y - point.y, 2)
				<= pow(c->radius, 2) )
		{
			return true;
		}
	}
	return false;
}

/* mf unlocked. */
static conductor_t * __attribute__((nonnull))
conductor_new ( mfield_t * const mf,
				const char * const name,
				const point_t at,
				const double radius,
				const double current )
{
	conductor_t * const conductor = malloc(sizeof(conductor_t));
	if ( conductor == NULL ) {
		perror("malloc");
		return NULL;
	}

	conductor->name = strdup(name);
	if ( conductor->name == NULL ) {
		perror("strdup");
		free(conductor);
		return NULL;
	}

	conductor->mf = mf;
	conductor->gtk = NULL;
	conductor->at = at;
	conductor->radius = radius;
	conductor->current = current;

	mf_lock(mf);
	conductor->next = mf->conductors;
	mf->conductors = conductor;
	mf_unlock(mf);

	mf_modified(mf);

	mf_redraw(mf);
	return conductor;
}

/* GTK lock. mf unlocked. */
static void __attribute__((nonnull (1)))
conductor_destroy ( conductor_t * const conductor )
{
	if ( conductor == NULL ) return;

	/* Destroy any attached GTK interface before destroying anything
	 * else. */
	if ( conductor->gtk != NULL ) {
		conductor_gtk_destroy(conductor->gtk);
	}

	mfield_t * const mf = conductor->mf;

	/* Unlink the conductor. */
	mf_lock(mf);
	if ( mf->conductors == conductor ) {
		mf->conductors = conductor->next;
	} else {
		conductor_t * c = mf->conductors;
		while ( c->next != conductor && c != NULL ) c = c->next;
		assert(c != NULL);
		c->next = conductor->next;
	}
	mf_unlock(mf);

	free(conductor->name);
	free(conductor);

	mf_modified(mf);

	mf_redraw(mf);
	return;
}

/* GTK lock. mf unlocked. */
static void __attribute__((nonnull))
conductors_destroy ( mfield_t * const mf )
{
	mf_lock(mf);
	while ( mf->conductors != NULL ) {
		mf_unlock(mf);
		conductor_destroy(mf->conductors);
		mf_lock(mf);
	}
	mf_unlock(mf);
	return;
}

/* mf unlocked. */
static conductor_gtk_t * 
conductor_gtk_new ( conductor_t * const conductor,
					const gchar * const name )
{
	mfield_t * const mf = conductor->mf;
	conductor_gtk_t * cg = NULL;

	cg = malloc(sizeof(conductor_gtk_t));
	if ( cg == NULL ) {
		perror("malloc");
		return NULL;
	}

	/* Link the two structs together. */
	cg->conductor = conductor;
	conductor->gtk = cg;

	cg->pq_current = NULL;
	cg->pq_radius = NULL;
	cg->vw_point = NULL;
	cg->hidden = true;
	cg->x = -1;
	cg->y = -1;

	cg->pq_current =
		pq_new(mf, &cg->conductor->current, "Current", "A");
	if ( cg->pq_current == NULL ) {
		goto error;
	}

	cg->pq_radius = pq_new(mf, &cg->conductor->radius, "Radius", "m");
	if ( cg->pq_radius == NULL ) {
		goto error;
	}
	pq_nonnegative(cg->pq_radius);

	cg->vw_point = vector_widget_new(mf, (vector_t *)&cg->conductor->at,
									 "Point", "m");
	if ( cg->vw_point == NULL ) {
		goto error;
	}

	/* A window to host the conductor properties. */
	cg->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	/* There is no need resizing this window. It just looks bad. */
	gtk_window_set_resizable(GTK_WINDOW(cg->window), FALSE);

	/* Set window title. */
	gchar * const title =
		g_strdup_printf("%s - Conductor %s", PACKAGE_NAME, name);
	gtk_window_set_title(GTK_WINDOW(cg->window), title);
	g_free(title);

	gtk_container_set_border_width(GTK_CONTAINER(cg->window), 3);
	g_signal_connect(cg->window, "delete-event",
					 G_CALLBACK(cb_cg_window_hide), cg);

	GtkWidget * vbox = gtk_vbox_new(FALSE, 5);
	gtk_container_add(GTK_CONTAINER(cg->window), vbox);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), cg->pq_current->hbox);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), cg->pq_radius->hbox);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), cg->vw_point->hbox);

	/* The HBox in the main window. */
	cg->hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start_defaults(GTK_BOX(mf->conductor_vbox), cg->hbox);

	GtkWidget * label = gtk_label_new(name);
	gtk_box_pack_start_defaults(GTK_BOX(cg->hbox), label);

	GtkWidget * button_box = gtk_hbutton_box_new();
	gtk_box_pack_start_defaults(GTK_BOX(cg->hbox), button_box);

	GtkWidget * button;
	button = gtk_button_new_from_stock(GTK_STOCK_EDIT);
	g_signal_connect(button, "clicked", G_CALLBACK(cb_cg_window_show),
					 cg);
	gtk_box_pack_start_defaults(GTK_BOX(button_box), button);

	button = gtk_button_new_from_stock(GTK_STOCK_DELETE);
	g_signal_connect(button, "clicked", G_CALLBACK(cb_delete_conductor),
					 cg);
	gtk_box_pack_start_defaults(GTK_BOX(button_box), button);

	gtk_widget_show_all(cg->hbox);
	gtk_widget_show_all(vbox);

	return cg;
error:
	if ( cg != NULL ) {
		vector_widget_destroy(cg->vw_point);
		pq_destroy(cg->pq_radius);
		pq_destroy(cg->pq_current);
		conductor_destroy(cg->conductor);
		free(cg);
	}
	return NULL;
}

static void __attribute__((nonnull))
conductor_gtk_destroy ( conductor_gtk_t * const cg )
{
	/* Remove the link. */
	cg->conductor->gtk = NULL;

	pq_destroy(cg->pq_current);
	pq_destroy(cg->pq_radius);
	vector_widget_destroy(cg->vw_point);
	gtk_widget_destroy(cg->hbox);
	gtk_widget_destroy(cg->window);
	free(cg);
	return;
}

/* mf unlocked. */
static void __attribute__((nonnull))
conductor_new_with_gtk ( mfield_t * const mf,
						 const char * const name,
						 const point_t at,
						 const double radius,
						 const double current )
{
	conductor_t * const conductor =
		conductor_new(mf, name, at, radius, current);
	if ( conductor == NULL ) {
		return;
	}

	conductor_gtk_t * const cg = conductor_gtk_new(conductor, name);
	if ( cg == NULL ) {
		conductor_destroy(conductor);
		return;
	}

	return;
}

/* Get the magnetic field in a point p. */
static vector_t
magnetic_field ( const mfield_t * const mf, const point_t p )
{
	vector_t b;

	b.x = mf->hmf.x;
	b.y = mf->hmf.y;
	for ( conductor_t * c = mf->conductors; c != NULL; c = c->next ) {
		const double factor = K * c->current /
			(pow(c->at.x-p.x, 2) + pow(c->at.y-p.y, 2));
		b.x += factor * (p.y - c->at.y);
		b.y -= factor * (p.x - c->at.x);
	}

	return b;
}

static void
on_close ( GLFWwindow * const window )
{
	mfield_t * const mf = glfwGetWindowUserPointer(window);

	mf_lock(mf);
	mf->quit = true;
	mf_unlock(mf);

	/* Prevent the window from closing. The window will be closed when
	 * GTK has decided to quit. */
	glfwSetWindowShouldClose(mf->window, GL_FALSE);

	return;
}

static void
refresh ( GLFWwindow * const window )
{
	mfield_t * const mf = glfwGetWindowUserPointer(window);
	mf_redraw(mf);
	return;
}

static void
on_resize ( GLFWwindow * const window,
			const int width,
			const int height )
{
	mfield_t * const mf = glfwGetWindowUserPointer(window);

	/* Update the spin buttons in the window. */
	gdk_threads_enter();
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(mf->width_spin), width);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(mf->height_spin), height);
	pxmc_set_px_max(mf->margin_pxmc, MIN(width, height));
	gdk_threads_leave();

	mf_lock(mf);
	mf->width = width;
	mf->height = height;
	mf_unlock(mf);
	mf_redraw(mf);

	return;
}

static void * __attribute__((nonnull))
gtk_thread_routine ( void * arg )
{
	mfield_t * const mf = (mfield_t *)arg;
	gdk_threads_enter();
	gtk_main();
	gdk_threads_leave();

	glfwSetWindowShouldClose(mf->window, GL_TRUE);

	/* Request a dummy-swap to push the main thread's loop. */
	glfwSwapBuffers(mf->window);

	return NULL;
}

/* Returns true if the caller can go on overwriting the file at filename
 * and false otherwise. */
static bool __attribute__((nonnull))
file_exists_dialog ( GtkWidget * parent, const char * const filename )
{
	const char * filename_short = strrchr(filename, '/');

	/* There should be a '/' in the string. */
	assert(filename_short != NULL);

	/* Step to the character after the '/', where the short filename
	 * begins. */
	filename_short = filename_short + 1;

	/* The filename should not end with a '/'. */
	assert(filename_short[0] != '\0');

	GtkWidget * dialog;
	dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
									GTK_DIALOG_MODAL,
									GTK_MESSAGE_QUESTION,
									GTK_BUTTONS_NONE,
									"A file named \"%s\" already "
									"exists. Do you want to replace "
									"it?",
									filename_short);
	gtk_window_set_title(GTK_WINDOW(dialog), "Question");
	gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
											 "Replacing it will "
											 "overwrite its contents.");
	gtk_dialog_add_button(GTK_DIALOG(dialog),
						  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);

	/* The "Replace"-button" */
	GtkWidget * button = gtk_button_new();
	GtkWidget * hbox = gtk_hbox_new(FALSE, 3);
	gtk_container_add(GTK_CONTAINER(button), hbox);
	GtkWidget * image = gtk_image_new_from_stock(GTK_STOCK_SAVE_AS,
												 GTK_ICON_SIZE_BUTTON);
	GtkWidget * label = gtk_label_new_with_mnemonic("_Replace");
	gtk_box_pack_start_defaults(GTK_BOX(hbox), image);
	gtk_box_pack_start_defaults(GTK_BOX(hbox), label);
	gtk_dialog_add_action_widget(GTK_DIALOG(dialog),
								 button, GTK_RESPONSE_YES);
	gtk_widget_show_all(button);

	gint ret = gtk_dialog_run(GTK_DIALOG(dialog));

	gtk_widget_destroy(dialog);

	return (ret == GTK_RESPONSE_YES);
}


/* Returns true if the file was saved and false otherwise. */
static bool __attribute__((nonnull))
save_as_dialog ( mfield_t * const mf )
{
	bool ret = false;

	GtkWidget * dialog;
	dialog = gtk_file_chooser_dialog_new("Save as",
										 GTK_WINDOW(mf->gtk_window),
										 GTK_FILE_CHOOSER_ACTION_SAVE,
										 GTK_STOCK_CANCEL,
										 GTK_RESPONSE_CANCEL,
										 GTK_STOCK_SAVE,
										 GTK_RESPONSE_ACCEPT,
										 NULL);
	gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog),
										 FALSE);

	GtkFileFilter * filter = gtk_file_filter_new();
	gtk_file_filter_set_name(filter, "Configuration files");
	gtk_file_filter_add_pattern(filter, "*" CONFIG_SUFFIX);
	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

	if ( gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT ) {
		gchar * filename = gtk_file_chooser_get_filename(
								GTK_FILE_CHOOSER(dialog));
		if ( filename != NULL ) {
			/* Check if the file exists and ask the user if he/she wants
			 * to overwrite if it exists. */
			if ( access(filename, F_OK) == 0 ) {
				/* The file exists! */
				if ( file_exists_dialog(dialog, filename) ) {
					goto save;
				}
			} else {
				/* The file does not exist. Go on saving. */
				goto save;
			}

			if ( false ) {
			save:
				if ( mf_save(mf, filename) ) {
					/* The file was saved. Return true. */
					ret = true;
				}
			}
			g_free(filename);
		}
	}

	gtk_widget_destroy(dialog);

	return ret;
}

/* GTK lock. mf unlocked.
 * Returns true if the file was saved, false otherwise. */
static bool __attribute__((nonnull))
save ( mfield_t * const mf )
{
	mf_lock(mf);
	if ( mf->save_filename != NULL ) {
		mf_unlock(mf);
		mf_save(mf, NULL);
		return true;
	} else {
		mf_unlock(mf);
		return save_as_dialog(mf);
	}
}

/* GTK lock. mf unlocked. */
static bool __attribute__((nonnull))
unsaved_dialog ( mfield_t * const mf )
{
	GtkWidget * dialog;
	dialog = gtk_message_dialog_new(GTK_WINDOW(mf->gtk_window),
									GTK_DIALOG_MODAL,
									GTK_MESSAGE_QUESTION,
									GTK_BUTTONS_NONE,
									/* FIXME: Filename here? */
									"The current configuration is not "
									"saved.");
	gtk_window_set_title(GTK_WINDOW(dialog), "Question");
	gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
											 "Do you want to save "
											 "before closing?");
	gtk_dialog_add_button(GTK_DIALOG(dialog),
						  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);

	/* The "Don't save"-button" */
	GtkWidget * button = gtk_button_new();
	GtkWidget * hbox = gtk_hbox_new(FALSE, 3);
	gtk_container_add(GTK_CONTAINER(button), hbox);
	GtkWidget * image = gtk_image_new_from_stock(GTK_STOCK_CLEAR,
												 GTK_ICON_SIZE_BUTTON);
	GtkWidget * label = gtk_label_new_with_mnemonic("_Don't save");
	gtk_box_pack_start_defaults(GTK_BOX(hbox), image);
	gtk_box_pack_start_defaults(GTK_BOX(hbox), label);
	gtk_dialog_add_action_widget(GTK_DIALOG(dialog),
								 button, GTK_RESPONSE_NO);
	gtk_widget_show_all(button);

	gtk_dialog_add_button(GTK_DIALOG(dialog),
						  GTK_STOCK_SAVE, GTK_RESPONSE_YES);

	gint ret = gtk_dialog_run(GTK_DIALOG(dialog));

	gtk_widget_destroy(dialog);

	switch ( ret ) {
		case GTK_RESPONSE_YES:
			if ( save(mf) ) {
				return true; /* The caller may go on. */
			} else {
				/* The file was not saved. The caller may not go on. */
				return false;
			}
		case GTK_RESPONSE_NO:
			return true; /* The caller may go on. */
		default:
			return false; /* The caller may not go on. */
	}
}

/* GTK lock. mf unlocked.
 * Returns true if it is OK to quit, false otherwise. */
static bool __attribute__((nonnull))
quit ( mfield_t * const mf )
{
	if ( !mf->saved ) {
		/* Ask the user what he/she wants to do. */
		if ( !unsaved_dialog(mf) ) {
			/* The user canceled the dialog. Don't go on. */
			return false;
		}
	}
	return true;
}

/* GTK unlocked. mf unlocked. */
static gboolean
handle_quit ( gpointer data )
{
	mfield_t * const mf = (mfield_t *)data;
	gdk_threads_enter();
	mf_lock(mf);
	if ( mf->quit ) {
		mf->quit = false;
		mf_unlock(mf);
		if ( quit(mf) ) {
			gtk_main_quit();
		}
	} else {
		mf_unlock(mf);
	}
	gdk_threads_leave();
	return TRUE;
}

static void
create_spin ( const char * const name,
			  const double min,
			  const double max,
			  const double step,
			  const double current,
			  GtkWidget ** hbox,
			  GtkWidget ** spin )
{
	*hbox = gtk_hbox_new(FALSE, 5);
	GtkWidget * label = gtk_label_new(name);
	*spin = gtk_spin_button_new_with_range(min, max, step);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(*spin), current);
	gtk_box_pack_start_defaults(GTK_BOX(*hbox), label);
	gtk_box_pack_start_defaults(GTK_BOX(*hbox), *spin);
	return;
}

static gboolean
cb_add_conductor ( GtkWidget * widget, gpointer data )
{
	mfield_t * const mf = (mfield_t *)data;

	gchar * const name = gtk_editable_get_chars(
							GTK_EDITABLE(mf->conductor_entry), 0, -1);
	if ( strcmp(name, "") == 0 ) {
		goto end;
	}
	gtk_entry_set_text(GTK_ENTRY(mf->conductor_entry), "");

	conductor_new_with_gtk(mf, name,
						   DEFAULT_CONDUCTOR_AT,
						   DEFAULT_CONDUCTOR_RADIUS,
						   DEFAULT_CONDUCTOR_CURRENT);

end:
	g_free(name);
	return FALSE;
}

static gboolean
cb_conductor_entry_changed ( GtkEditable * editable,
							 gpointer data )
{
	GtkWidget * button = (GtkWidget *)data;
	gchar * const text = gtk_editable_get_chars(editable, 0, -1);
	if ( strlen(text) > 0 ) {
		gtk_widget_set_sensitive(button, TRUE);
	} else {
		gtk_widget_set_sensitive(button, FALSE);
	}
	g_free(text);
	return FALSE;
}

/* GTK lock. mf unlocked. */
static gboolean
cb_delete_conductor ( GtkWidget * widget, gpointer data )
{
	conductor_gtk_t * const cg = (conductor_gtk_t *)data;
	conductor_t * const c = cg->conductor;

	conductor_gtk_destroy(cg);
	conductor_destroy(c);

	return FALSE;
}

/* GTK lock. mf unlocked. */
static gboolean
cb_draw_axes_check_toggled ( GtkToggleButton * toggle, gpointer data )
{
	mfield_t * const mf = (mfield_t *)data;
	mf_lock(mf);
	mf->draw_axes = gtk_toggle_button_get_active(toggle);
	mf_unlock(mf);
	mf_modified(mf);
	mf_redraw(mf);
	return FALSE;
}

/* GTK lock. mf unlocked. */
static gboolean
cb_new ( GtkMenuItem * menu_item, gpointer data )
{
	mfield_t * const mf = (mfield_t *)data;

	if ( !mf->saved ) {
		/* The current configuration is not saved. Ask the user whether
		 * he/she wants continue. */
		if ( !unsaved_dialog(mf) ) {
			/* The user canceled the dialog. Don't go on. */
			return TRUE;
		}
	}

	mf_load_defaults(mf);

	statusbar_display("New configuration.");

	mf_redraw(mf);

	return FALSE;
}

/* GTK lock. mf unlocked. */
static gboolean
cb_open ( GtkMenuItem * menu_item, gpointer data )
{
	mfield_t * const mf = (mfield_t *)data;

	/* Make sure nothing is overwritten without notice to the user. */
	if ( !mf->saved ) {
		/* Ask the user what he/she wants to do. */
		if ( !unsaved_dialog(mf) ) {
			/* The user canceled the dialog. Don't go on. */
			return TRUE;
		}
	}

	GtkWidget * dialog;
	dialog = gtk_file_chooser_dialog_new("Open",
										 GTK_WINDOW(mf->gtk_window),
										 GTK_FILE_CHOOSER_ACTION_OPEN,
										 GTK_STOCK_CANCEL,
										 GTK_RESPONSE_CANCEL,
										 GTK_STOCK_OPEN,
										 GTK_RESPONSE_ACCEPT,
										 NULL);
	gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog),
										 FALSE);
	GtkFileFilter * filter = gtk_file_filter_new();
	gtk_file_filter_set_name(filter, "Configuration files");
	gtk_file_filter_add_pattern(filter, "*" CONFIG_SUFFIX);
	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

	if ( gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT ) {
		gchar * filename = gtk_file_chooser_get_filename(
								GTK_FILE_CHOOSER(dialog));
		if ( filename != NULL ) {
			if ( mf_open(mf, filename) ) {
				statusbar_display_fmt("%s opened.",
									  mf->save_filename_short);
			}
		}
		g_free(filename);
	}

	gtk_widget_destroy(dialog);

	return FALSE;
}

static gboolean
cb_save ( GtkMenuItem * menu_item, gpointer data )
{
	mfield_t * const mf = (mfield_t *)data;
	if ( save(mf) ) {
		statusbar_display_fmt("%s saved.", mf->save_filename_short);
	}
	return FALSE;
}

static gboolean
cb_save_as ( GtkMenuItem * menu_item, gpointer data )
{
	mfield_t * const mf = (mfield_t *)data;
	if ( save_as_dialog(mf) ) {
		statusbar_display_fmt("%s saved.", mf->save_filename_short);
	}
	return FALSE;
}

static gboolean
cb_show_about_dialog ( GtkWidget * widget, gpointer data )
{
	mfield_t * const mf = (mfield_t *)data;
	gtk_dialog_run(GTK_DIALOG(mf->about_dialog));
	gtk_widget_hide(mf->about_dialog);
	return TRUE;
}

/* GTK lock. mf unlocked. */
static gboolean
cb_wh_spin_changed ( GtkSpinButton * spin, gpointer data )
{
	mfield_t * const mf = (mfield_t *)data;

	mf_lock(mf);
	mf->width =
		gtk_spin_button_get_value(GTK_SPIN_BUTTON(mf->width_spin));
	mf->height =
		gtk_spin_button_get_value(GTK_SPIN_BUTTON(mf->height_spin));
	mf_unlock(mf);

	/* Special behaviour to avoid unnecessary unsaved dialogs when only
	 * the width and/or height have changed and no file is open. */
	if ( mf->save_filename != NULL ) {
		mf_modified(mf);
	}

	/* Redrawing the window with new width and height will trigger the
	 * main thread to wake up and trigger yet another redraw. This
	 * approach is better than requesting manual action from the user.
	 */
	mf_redraw(mf);

	return TRUE;
}

static gboolean
cb_cg_window_hide ( GtkWidget * window,
					GdkEvent * event,
					gpointer data )
{
	conductor_gtk_t * const cg = (conductor_gtk_t *)data;

	if ( !cg->hidden ) {
		gtk_window_get_position(GTK_WINDOW(window), &cg->x, &cg->y);
		gtk_widget_hide(window);
		cg->hidden = true;
	}

	return TRUE;
}

static gboolean
cb_cg_window_show ( GtkWidget * widget, gpointer data )
{
	conductor_gtk_t * const cg = (conductor_gtk_t *)data;

	if ( cg->hidden ) {
		gtk_widget_show(cg->window);
		if ( cg->x != -1 && cg->y != -1 ) {
			gtk_window_move(GTK_WINDOW(cg->window), cg->x, cg->y);
		}
		cg->hidden = false;
	}

	return FALSE;
}

/* GTK lock. mf unlocked. */
static gboolean
cb_quit ( GtkWidget * widget, gpointer data )
{
	mfield_t * mf = (mfield_t *)data;

	if ( quit(mf) ) {
		gtk_main_quit();
	}

	return TRUE;
}

static gboolean
cb_window_close ( GtkWidget * widget, GdkEvent * event, gpointer data )
{
	/* Just wrap this one. */
	return cb_quit(widget, data);
}

static void *
draw_thread_routine ( void * arg )
{
	mfield_t * const mf = (mfield_t *)arg;

	glfwMakeContextCurrent(mf->window);

	/* Variables that will be extracted from the mf. It is a reasonably
	 * valid assumption that none of these are INFINITY or INT_MIN
	 * initially. */
	int width = DEFAULT_WIDTH;
	int height = DEFAULT_HEIGHT;
	double x_min = DEFAULT_X_MIN;
	double x_max = DEFAULT_X_MAX;
	double y_min = DEFAULT_Y_MIN;
	double y_max = DEFAULT_Y_MAX;

	/* Necessary variables. */
	double x_step;
	double y_step;
	double x_spacing;
	double y_spacing;
	double x_start;
	double y_start;
	double x_end;
	double y_end;

	/* The minimum space between a conductor and an arrow. */
	bool define_margin_in_px = DEFAULT_DEFINE_MARGIN_IN_PX;
	double margin_m = DEFAULT_MARGIN_M;
	unsigned int margin_px = DEFAULT_MARGIN_PX;
	double margin;

	/* The maximum arrow length. */
	bool define_arrow_len_max_in_px =
		DEFAULT_DEFINE_ARROW_LEN_MAX_IN_PX;
	double arrow_len_max_m = DEFAULT_ARROW_LEN_MAX_M;
	unsigned int arrow_len_max_px = DEFAULT_ARROW_LEN_MAX_PX;
	double arrow_len_max_x;
	double arrow_len_max_y;

	/* The space between the arrows. */
	bool define_arrow_spacing_in_px =
		DEFAULT_DEFINE_ARROW_SPACING_IN_PX;
	double arrow_spacing_m = DEFAULT_ARROW_SPACING_M;
	unsigned int arrow_spacing_px = DEFAULT_ARROW_SPACING_PX;
	double arrow_spacing_x;
	double arrow_spacing_y;

	/* This value could be anything because it is set to the correct
	 * value if it is wrong. */
	bool draw_axes = DEFAULT_DRAW_AXES;

	bool recalculate = true;
	while ( mf_draw_thread_loop(mf) ) {

		mf_lock(mf);
		if ( x_min != mf->x_min || x_max != mf->x_max ||
			 y_min != mf->y_min || y_max != mf->y_max ||
			 width != mf->width || height != mf->height ||
			 draw_axes != mf->draw_axes ||
			 define_margin_in_px != mf->define_margin_in_px ||
			 margin_px != mf->margin_px || margin_m != mf->margin_m ||
			 define_arrow_len_max_in_px !=
				mf->define_arrow_len_max_in_px ||
			 arrow_len_max_m != mf->arrow_len_max_m ||
			 arrow_len_max_px != mf->arrow_len_max_px ||
			 define_arrow_spacing_in_px !=
				mf->define_arrow_spacing_in_px ||
			 arrow_spacing_m != mf->arrow_spacing_m ||
			 arrow_spacing_px != mf->arrow_spacing_px )
		{
			/* If anything is changed (which it is going to be) a
			 * recalculation of the boundaries has to be made. */
			recalculate = true;

			/* Get the framebuffer size and see if it corresponds to the
			 * size specified in mf. If the size does not match, wake up
			 * the main thread by dummy-swapping the buffers so it can
			 * change the framebffer size. This clause will then be run
			 * once more. */
			glfwGetFramebufferSize(mf->window, &width, &height);
			if ( width != mf->width || height != mf->height ) {
				/* Do a dummy-swap to wake up the main thread. */
				glfwSwapBuffers(mf->window);
				mf_unlock(mf);
				continue;
			}
			
			/* Extract all the other new values. */
			draw_axes = mf->draw_axes;
			x_min = mf->x_min;
			x_max = mf->x_max;
			y_min = mf->y_min;
			y_max = mf->y_max;

			define_margin_in_px = mf->define_margin_in_px;
			margin_m = mf->margin_m;
			margin_px = mf->margin_px;

			define_arrow_len_max_in_px = mf->define_arrow_len_max_in_px;
			arrow_len_max_m = mf->arrow_len_max_m;
			arrow_len_max_px = mf->arrow_len_max_px;

			define_arrow_spacing_in_px = mf->define_arrow_spacing_in_px;
			arrow_spacing_m = mf->arrow_spacing_m;
			arrow_spacing_px = mf->arrow_spacing_px;
		}
		mf_unlock(mf);

		if ( recalculate ) {
			/* Don't attempt to recalculate again unless something else
			 * changes. */
			recalculate = false;

			/* Make sure the window boundaries are correct. */
			glViewport(0, 0, width, height);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluOrtho2D(x_min, x_max, y_min, y_max);
			glMatrixMode(GL_MODELVIEW);

			x_step = (x_max - x_min) / width;
			y_step = (y_max - y_min) / height;

			if ( define_arrow_len_max_in_px ) {
				arrow_len_max_x = arrow_len_max_px * x_step;
				arrow_len_max_y = arrow_len_max_px * y_step;
			} else {
				arrow_len_max_x = arrow_len_max_m;
				arrow_len_max_y = arrow_len_max_m;
			}

			if ( arrow_len_max_x == 0 ) {
				arrow_len_max_x = x_step;
			}
			if ( arrow_len_max_y == 0 ) {
				arrow_len_max_y = y_step;
			}

			if ( define_arrow_spacing_in_px ) {
				arrow_spacing_x = arrow_spacing_px * x_step;
				arrow_spacing_y = arrow_spacing_px * y_step;
			} else {
				arrow_spacing_x = arrow_spacing_m;
				arrow_spacing_y = arrow_spacing_m;
			}

			x_spacing = arrow_len_max_x + arrow_spacing_x;
			y_spacing = arrow_len_max_y + arrow_spacing_y;

			/* Prefer aligning the points where the vectors are
			 * calculated to the axes */
			if ( x_min < 0 && x_max > 0 ) {
				x_start = 0;
				while ( x_start - x_spacing >= x_min ) {
					x_start -= x_spacing;
				}

				x_end = 0;
				while ( x_end + x_spacing <= x_max ) {
					x_end += x_spacing;
				}
			} else {
				x_start = x_min;
				x_end = x_max;
			}

			if ( y_min < 0 && y_max > 0 ) {
				y_start = 0;
				while ( y_start - y_spacing >= y_min ) {
					y_start -= y_spacing;
				}

				y_end = 0;
				while ( y_end + y_spacing <= y_max ) {
					y_end += y_spacing;
				}
			} else {
				y_start = y_min;
				y_end = y_max;
			}

			if ( define_margin_in_px ) {
				if ( y_step > x_step ) {
					margin = margin_px * y_step;
				} else {
					margin = margin_px * x_step;
				}
			} else {
				margin = margin_m;
			}
		}

		glClearColor(0,0,0,0);
		glClear(GL_COLOR_BUFFER_BIT);

		double len_max = 0;

		point_t p;
		for ( p.x = x_start; p.x <= x_end; p.x += x_spacing ) {
			for ( p.y = y_start; p.y <= y_end; p.y += y_spacing ) {
				mf_lock(mf);
				if ( point_in_conductor(mf, p) ) {
					mf_unlock(mf);
					continue;
				}
				const vector_t v = magnetic_field(mf, p);
				mf_unlock(mf);

				const double len = len_vector(v);
				if ( len > len_max ) {
					len_max = len;
				}
			}
		}

		if ( len_max != 0 ) {
			for ( p.x = x_start; p.x <= x_end; p.x += x_spacing ) {
				for ( p.y = y_start; p.y <= y_end; p.y += y_spacing ) {
					mf_lock(mf);
					if ( point_in_conductor(mf, p) ) {
						mf_unlock(mf);
						continue;
					}
					const vector_t v = magnetic_field(mf, p);
					mf_unlock(mf);

					const double len = len_vector(v);
					if ( len == 0 ) {
						continue;
					}

					point_t start;
					start.x = p.x - v.x / len * arrow_len_max_x / 2;
					start.y = p.y - v.y / len * arrow_len_max_y / 2;

					point_t end;
					end.x = p.x + v.x / len * x_spacing / 2;
					end.y = p.y + v.y / len * y_spacing / 2;

					/* Make sure the arrow does not collide with a
					 * conductor. */
					mf_lock(mf);
					for ( const conductor_t * c = mf->conductors;
						  c != NULL;
						  c = c->next )
					{
						if ( line_segment_collides_with_circle(start,
								end, c->at, c->radius + margin) )
						{
							mf_unlock(mf);
							goto next;
						}
					}
					mf_unlock(mf);

					const int alpha = len/len_max * INT_MAX;
					glColor4i(INT_MAX, INT_MAX, INT_MAX, alpha);
					draw_arrow(start, end);
				next:
					;
				}
			}
		}

		draw_conductors(mf);

		if ( draw_axes ) {
			/* Draw coordinate axes. */
			glColor4i(INT_MAX, INT_MAX, INT_MAX, INT_MAX);
			glBegin(GL_LINES);
			glVertex2f(x_min, 0);
			glVertex2f(x_max, 0);
			glVertex2f(0, y_min);
			glVertex2f(0, y_max);
			glEnd();
		}

		glfwSwapBuffers(mf->window);

	}

	return NULL;
}

static inline void
create_about_dialog ( mfield_t * const mf )
{
	mf->about_dialog = gtk_about_dialog_new();

	/* Convenience variable. */
	GtkAboutDialog * about = GTK_ABOUT_DIALOG(mf->about_dialog);
	gtk_about_dialog_set_version(about, PACKAGE_VERSION);
	gtk_about_dialog_set_copyright(about,
		"Copyright (C) 2013 Karl Lindén <lilrc@users.sourceforge.net>");
	gtk_about_dialog_set_comments(about,
		"A program to display the magnetic field in a plane with "
		"circular conductors perpendicular to the plane using a vector "
		"field."
#if BUILDING_FROM_GIT
		"\nBuilt from git commit " GIT_COMMIT "."
#endif /* BUILDING_FROM_GIT */
		);
	gtk_about_dialog_set_license(about, LICENSE);
	gtk_about_dialog_set_website(about, PACKAGE_URL);
	const gchar * authors[] = {
		"Karl Lindén <lilrc@users.sourceforge.net>",
		NULL
	}; 
	gtk_about_dialog_set_authors(about, authors);
	return;
}

int
main ( int argc, char ** argv )
{
	int exit_status = EXIT_SUCCESS;

	mfield_t * const mf = mf_new();
	if ( mf == NULL ) {
		return EXIT_FAILURE;
	}

	/* These are the only default values that are not loaded in
	 * mf_load_defaults(). */
	mf->width = DEFAULT_WIDTH;
	mf->height = DEFAULT_HEIGHT;

	mf_load_defaults(mf);

	/* Set up GLFW. */
	if ( !glfwInit() ) {
		mf_destroy(mf);
		return EXIT_FAILURE;
	}

	mf->window = glfwCreateWindow(mf->width, mf->height, PACKAGE_NAME,
								  NULL, NULL);
	if ( mf->window == NULL ) {
		goto error;
	}

	glfwSetWindowUserPointer(mf->window, mf);
	glfwMakeContextCurrent(mf->window);

	glfwGetFramebufferSize(mf->window, &mf->width, &mf->height);

	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glViewport(0, 0, mf->width, mf->height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(mf->x_min, mf->x_max, mf->y_min, mf->y_max);
	glMatrixMode(GL_MODELVIEW);

	/* Set up GTK. */
	gdk_threads_init();
	gtk_init(&argc, &argv);

	/* Some common widgets. */
	GtkWidget * button;
	GtkWidget * button_box;
	GtkWidget * check;
	GtkWidget * frame;
	GtkWidget * label;
	GtkWidget * hbox;
	GtkWidget * vbox;
	GtkWidget * page_vbox;
	GtkWidget * menu;
	GtkWidget * menu_item;

	create_about_dialog(mf);

	mf->gtk_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(mf->gtk_window), PACKAGE_NAME);
	g_signal_connect(mf->gtk_window, "delete-event",
					 G_CALLBACK(cb_window_close), mf);

	GtkAccelGroup * accel = gtk_accel_group_new();
	gtk_window_add_accel_group(GTK_WINDOW(mf->gtk_window), accel);

	vbox = gtk_vbox_new(FALSE, 5);
	gtk_container_add(GTK_CONTAINER(mf->gtk_window), vbox);

	GtkWidget * menu_bar = gtk_menu_bar_new();
	gtk_box_pack_start(GTK_BOX(vbox), menu_bar, FALSE, FALSE, 0);

	menu_item = gtk_menu_item_new_with_mnemonic("_File");
	menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), menu);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), menu_item);

	menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_NEW,
												   accel);
	g_signal_connect(menu_item, "activate", G_CALLBACK(cb_new), mf);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);

	menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_OPEN,
												   accel);
	g_signal_connect(menu_item, "activate", G_CALLBACK(cb_open), mf);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);

	menu_item = gtk_separator_menu_item_new();
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);

	menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_SAVE,
												   accel);
	g_signal_connect(menu_item, "activate", G_CALLBACK(cb_save), mf);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);

	menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_SAVE_AS,
												   accel);
	g_signal_connect(menu_item, "activate", G_CALLBACK(cb_save_as), mf);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);

	menu_item = gtk_separator_menu_item_new();
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);

	menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_QUIT,
												   accel);
	g_signal_connect(menu_item, "activate", G_CALLBACK(cb_quit), mf);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);

	menu_item = gtk_menu_item_new_with_mnemonic("_Help");
	menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), menu);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), menu_item);

	menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_ABOUT,
												   accel);
	g_signal_connect(menu_item, "activate",
					 G_CALLBACK(cb_show_about_dialog), mf);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);

	GtkWidget * notebook = gtk_notebook_new();
	gtk_box_pack_start_defaults(GTK_BOX(vbox), notebook);

	GtkWidget * statusbar = gtk_statusbar_new();
	gtk_box_pack_start(GTK_BOX(vbox), statusbar, FALSE, FALSE, 0);

	mf->statusbar = GTK_STATUSBAR(statusbar);

	mf->context_id = gtk_statusbar_get_context_id(mf->statusbar, "");
	statusbar_display("Welcome! This is mfield. New configuration.");

	/* The magnetic field page. */
	label = gtk_label_new("Main");
	page_vbox = gtk_vbox_new(FALSE, 5);
	gtk_container_set_border_width(GTK_CONTAINER(page_vbox), 3);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), page_vbox, label);

	frame = gtk_frame_new("Homogenous magnetic field");
	gtk_box_pack_start_defaults(GTK_BOX(page_vbox), frame);
	vbox = gtk_vbox_new(TRUE, 5);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 3);
	gtk_container_add(GTK_CONTAINER(frame), vbox);

	vector_widget_t * vw =
		vector_widget_new(mf, &mf->hmf, "Magnetic field", "T");
	gtk_box_pack_start_defaults(GTK_BOX(vbox), vw->hbox);

	frame = gtk_frame_new("Conductors");
	gtk_box_pack_start_defaults(GTK_BOX(page_vbox), frame);

	vbox = gtk_vbox_new(FALSE, 5);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 3);
	gtk_container_add(GTK_CONTAINER(frame), vbox);

	/* To be filled in when the user adds conductors. */
	mf->conductor_vbox = gtk_vbox_new(TRUE, 5);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), mf->conductor_vbox);

	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), hbox);

	label = gtk_label_new("Name:");
	gtk_box_pack_start_defaults(GTK_BOX(hbox), label);

	mf->conductor_entry = gtk_entry_new();
	gtk_box_pack_start_defaults(GTK_BOX(hbox), mf->conductor_entry);

	button_box = gtk_hbutton_box_new();
	gtk_box_pack_start_defaults(GTK_BOX(hbox), button_box);

	button = gtk_button_new_from_stock(GTK_STOCK_ADD);
	gtk_widget_set_sensitive(button, FALSE);
	g_signal_connect(mf->conductor_entry, "activate",
					 G_CALLBACK(cb_add_conductor), mf);
	g_signal_connect(mf->conductor_entry, "changed",
					 G_CALLBACK(cb_conductor_entry_changed), button);
	g_signal_connect(button, "clicked", G_CALLBACK(cb_add_conductor),
					 mf);
	gtk_box_pack_start_defaults(GTK_BOX(button_box), button);

	/* The vector field notebook page. */
	label = gtk_label_new("Vector field");
	page_vbox = gtk_vbox_new(FALSE, 5);
	gtk_container_set_border_width(GTK_CONTAINER(page_vbox), 3);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), page_vbox, label);

	frame = gtk_frame_new("Arrows");
	gtk_box_pack_start_defaults(GTK_BOX(page_vbox), frame);

	vbox = gtk_vbox_new(FALSE, 5);
	gtk_container_add(GTK_CONTAINER(frame), vbox);

	pxmc_t * pxmc;
	pxmc = pxmc_new(mf,
					&mf->define_arrow_len_max_in_px,
					&mf->arrow_len_max_m,
					&mf->arrow_len_max_px,
					"Maximum arrow length");
	if ( pxmc == NULL ) {
		goto error;
	}
	gtk_box_pack_start_defaults(GTK_BOX(vbox), pxmc->vbox);

	GtkWidget * separator = gtk_hseparator_new();
	gtk_box_pack_start_defaults(GTK_BOX(vbox), separator);

	pxmc = pxmc_new(mf,
					&mf->define_arrow_spacing_in_px,
					&mf->arrow_spacing_m,
					&mf->arrow_spacing_px,
					"Space between arrows");
	if ( pxmc == NULL ) {
		goto error;
	}
	gtk_box_pack_start_defaults(GTK_BOX(vbox), pxmc->vbox);

	frame = gtk_frame_new(NULL);
	gtk_box_pack_start_defaults(GTK_BOX(page_vbox), frame);

	mf->margin_pxmc = pxmc_new(mf, 
							   &mf->define_margin_in_px,
							   &mf->margin_m,
							   &mf->margin_px,
							   "Minimum space between a conductor and "
							   "an arrow");
	if ( mf->margin_pxmc == NULL ) {
		goto error;
	}
	pxmc_set_px_max(mf->margin_pxmc, MIN(mf->width, mf->height));
	gtk_container_add(GTK_CONTAINER(frame), mf->margin_pxmc->vbox);

	check = gtk_check_button_new_with_label("Draw coordinate axes");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check),
								 mf->draw_axes);
	g_signal_connect(check, "toggled",
					 G_CALLBACK(cb_draw_axes_check_toggled), mf);
	gtk_box_pack_start_defaults(GTK_BOX(page_vbox), check);

	/* The window notebook page. */
	label = gtk_label_new("Window");
	page_vbox = gtk_vbox_new(FALSE, 5);
	gtk_container_set_border_width(GTK_CONTAINER(page_vbox), 3);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), page_vbox, label);

	frame = gtk_frame_new("Size");
	gtk_box_pack_start_defaults(GTK_BOX(page_vbox), frame);
	vbox = gtk_vbox_new(TRUE, 5);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 3);
	gtk_container_add(GTK_CONTAINER(frame), vbox);

	int width_max = mf->width;
	int height_max = mf->height;
	int monitor_count;
	GLFWmonitor ** const monitors = glfwGetMonitors(&monitor_count);
	for ( int i = 0; i < monitor_count; ++i ) {
		/* vm for videomode. */
		const GLFWvidmode * const vm = glfwGetVideoMode(monitors[i]);
		if ( vm == NULL ) {
			goto error;
		}
		if ( vm->width > width_max ) {
			width_max = vm->width;
		}
		if ( vm->height > height_max ) {
			height_max = vm->height;
		}
	}

	create_spin("Width", 1, width_max, 1, mf->width, &hbox,
						  &mf->width_spin);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), hbox);
	g_signal_connect(mf->width_spin, "value-changed",
					 G_CALLBACK(cb_wh_spin_changed), mf);

	create_spin("Height", 1, height_max, 1, mf->height, &hbox,
						  &mf->height_spin);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), hbox);
	g_signal_connect(mf->height_spin, "value-changed",
					 G_CALLBACK(cb_wh_spin_changed), mf);

	frame = gtk_frame_new("Boundaries");
	gtk_box_pack_start_defaults(GTK_BOX(page_vbox), frame);
	vbox = gtk_vbox_new(TRUE, 5);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 3);
	gtk_container_add(GTK_CONTAINER(frame), vbox);

	phys_quant_t * pq1;
	phys_quant_t * pq2;
	pq1 = pq_new(mf, &mf->x_min, "x-min", "m");
	pq2 = pq_new(mf, &mf->x_max, "x-max", "m");
	pq_set_less(pq2, pq1);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), pq1->hbox);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), pq2->hbox);

	pq1 = pq_new(mf, &mf->y_min, "y-min", "m");
	pq2 = pq_new(mf, &mf->y_max, "y-max", "m");
	pq_set_less(pq2, pq1);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), pq1->hbox);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), pq2->hbox);

	gtk_widget_show_all(mf->gtk_window);
	pxmc_hide_and_show_all(mf);

	GtkRequisition req;
	gtk_widget_size_request(mf->gtk_window, &req);
	gtk_window_resize(GTK_WINDOW(mf->gtk_window),
					  req.width, req.height);

	/* Add necessary callbacks before entering the main loop. */
	glfwSetWindowCloseCallback(mf->window, on_close);
	glfwSetWindowRefreshCallback(mf->window, refresh);
	glfwSetWindowSizeCallback(mf->window, on_resize);

	/* The GL thread (the main thread) cannot ruin the quit mechanisms
	 * as that would block gtk's main loop which would make the program
	 * look hung-up. That is the reason this function needs to be called
	 * each second to check whether the GL thread has requested the
	 * program to close. */
	g_timeout_add_seconds(1, handle_quit, mf);

	if ( mf_start_draw_thread(mf) ) {
		goto error;
	}

	/* Start the GTK main loop in a secondary thread. */
	pthread_t gtk_thread;
	pthread_attr_t attr;
	if ( pthread_attr_init(&attr) ) {
		perror("pthread_attr_init");
		goto error;
	}

	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	if ( pthread_create(&gtk_thread, &attr, &gtk_thread_routine, mf) )
	{
		perror("pthread_create");
		pthread_attr_destroy(&attr);
		goto error;
	}

	pthread_attr_destroy(&attr);

	/* Run the main loop! */
	int width = mf->width;
	int height = mf->height;
	while ( !glfwWindowShouldClose(mf->window) ) {
		mf_lock(mf);
		if ( width != mf->width || height != mf->height ) {
			glfwSetWindowSize(mf->window, mf->width, mf->height);
			glfwGetFramebufferSize(mf->window, &mf->width, &mf->height);
			width = mf->width;
			height = mf->height;
			mf_unlock(mf);

			gdk_threads_enter();
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(mf->width_spin), width);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(mf->height_spin), height);
			gdk_threads_leave();
		} else {
			mf_unlock(mf);
		}

		glfwWaitEvents();
	}

	pthread_join(gtk_thread, NULL);

	mf_stop_draw_thread(mf);

	if ( false ) {
	error:
		exit_status = EXIT_FAILURE;
	}

	glfwTerminate();

	conductors_destroy(mf);
	pxmc_destroy_all(mf);
	vector_widget_destroy_all(mf);
	pq_destroy_all(mf);
	mf_destroy(mf);

	return exit_status;
}
