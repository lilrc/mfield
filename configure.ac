#  
#  This file is part of mfield.
#
#  Copyright (C) Karl Lindén <lilrc@users.sourceforge.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.69])
AC_INIT([mfield],
		[0.0.2],
		[https://bitbucket.org/lilrc/mfield/issues],
		[mfield],
		[https://bitbucket.org/lilrc/mfield])
AC_CONFIG_SRCDIR([mfield.c])
AC_CONFIG_HEADERS([config.h])

AM_INIT_AUTOMAKE

# Checks for programs.
AC_PROG_CC
AC_PROG_CC_C99
AS_IF(
	[test x$ac_cv_prog_cc_c99 = xno],
	[AC_MSG_ERROR([A C99 compatible C compiler is required])])

AM_PROG_CC_C_O

m4_ifndef([PKG_PROG_PKG_CONFIG], [m4_fatal([pkg-config is required])])
PKG_PROG_PKG_CONFIG

AC_ARG_VAR([GIT], [The git program])
AC_CHECK_PROGS([GIT], [git])
AS_IF(
	[test -n $GIT && test -d $srcdir/.git],
	[AC_DEFINE(
		[BUILDING_FROM_GIT],
		[1],
		[Define if this build is built from git.])])
AM_CONDITIONAL(
	[BUILDING_FROM_GIT],
	[test -n $GIT && test -d $srcdir/.git])

# Checks for libraries.
AC_SEARCH_LIBS([sqrt], [m])

PKG_CHECK_MODULES(
	[gl], [gl],
	[:],
	[AC_MSG_ERROR([GL is required])])
PKG_CHECK_MODULES(
	[glu], [glu],
	[:],
	[AC_MSG_ERROR([GLU is required])])
PKG_CHECK_MODULES(
	[glfw], [glfw3],
	[:],
	[AC_MSG_ERROR([GLFW3 is required])])

PKG_CHECK_MODULES(
	[gtk], [gtk+-2.0],
	[:],
	[AC_MSG_ERROR([GTK+2 is required])])

PKG_CHECK_MODULES(
	[libconfig], [libconfig],
	[:],
	[AC_MSG_ERROR([libconfig is required])])

# Checks for header files.
AC_CHECK_HEADERS([assert.h limits.h math.h pthread.h stdarg.h stdio.h])
AC_CHECK_HEADERS([stdlib.h string.h unistd.h])
AC_HEADER_ASSERT
AC_HEADER_STDBOOL

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_C_INLINE

# Checks for library functions.
AC_FUNC_MALLOC
AC_CHECK_FUNCS([access pow sqrt])

AC_CONFIG_FILES([
	doc/Makefile
	Makefile
])
AC_OUTPUT
